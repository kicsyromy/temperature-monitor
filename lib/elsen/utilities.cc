#include "utilities.hh"

#include <FreeRTOS.h>
#include <driver/gpio.h>
#include <freertos/task.h>

#include <esp_timer.h>

#include <cctype>

namespace elsen::utilities
{
    void delay(std::chrono::microseconds value) noexcept
    {
        if (value.count() <= 0)
        {
            return;
        }

        auto       value_microseconds = static_cast<std::uint64_t>(value.count());
        const auto m                  = static_cast<std::uint64_t>(esp_timer_get_time());

        const auto e = uint64_t{ m + value_microseconds };
        if (m > e)
        {
            while (static_cast<std::uint64_t>(esp_timer_get_time()) > e)
            {
                __asm volatile("nop");
            }
        }
        while (static_cast<std::uint64_t>(esp_timer_get_time()) < e)
        {
            __asm volatile("nop");
        }
    }

    void delay(std::chrono::milliseconds value) noexcept
    {
        vTaskDelay(static_cast<TickType_t>(value.count() / portTICK_PERIOD_MS));
    }

    void set_pin_state(std::uint8_t pin, PinState value) noexcept
    {
        const auto result = gpio_set_level(static_cast<gpio_num_t>(pin), value);
        ESP_ERROR_CHECK(result);
    }

    PinState pin_state(std::uint8_t pin) noexcept
    {
        return static_cast<PinState>(gpio_get_level(static_cast<gpio_num_t>(pin)));
    }

    void set_pin_mode(std::uint8_t pin, PinMode mode) noexcept
    {
        static_assert(static_cast<std::int32_t>(INPUT) ==
                      static_cast<std::int32_t>(GPIO_MODE_INPUT));
        static_assert(static_cast<std::int32_t>(OUTPUT) ==
                      static_cast<std::int32_t>(GPIO_MODE_OUTPUT));

        gpio_config_t io_conf{};
        io_conf.intr_type    = GPIO_INTR_DISABLE;
        io_conf.mode         = static_cast<gpio_mode_t>(mode);
        io_conf.pin_bit_mask = (1ULL << pin);
        io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
        io_conf.pull_up_en   = GPIO_PULLUP_DISABLE;

        const auto result = gpio_config(&io_conf);
        ESP_ERROR_CHECK(result);
    }

    std::string_view trimmed(std::string_view in)
    {
        auto left = in.begin();
        for (;; ++left)
        {
            if (left == in.end())
                return std::string_view();
            if (!isspace(*left))
                break;
        }
        auto right = in.end() - 1;
        for (; right > left && isspace(*right); --right)
            ;
        return { left, static_cast<std::size_t>(std::distance(left, right) + 1) };
    }
} // namespace elsen::utilities