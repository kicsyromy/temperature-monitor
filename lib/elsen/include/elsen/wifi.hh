#pragma once

#include <elsen/signal.hh>

#include <FreeRTOS.h>
#include <freertos/event_groups.h>

#include <esp_event.h>
#include <esp_wifi.h>

#include <array>
#include <cstdint>
#include <memory>
#include <string>
#include <type_traits>

namespace elsen
{
    namespace wifi
    {
        struct Mode
        {
            enum Values : std::uint8_t
            {
                AccessPoint = 0b00000001,
                Station     = 0b00000010,
            };
            using Flags = std::underlying_type_t<Values>;
        };

        enum struct AuthenticationMode
        {
            Open        = WIFI_AUTH_OPEN,
            Wep         = WIFI_AUTH_WEP,
            Wpa2Psk     = WIFI_AUTH_WPA2_PSK,
            WpaWpa2Psk  = WIFI_AUTH_WPA_WPA2_PSK,
            Wpa3Psk     = WIFI_AUTH_WPA3_PSK,
            Wpa2Wpa3Psk = WIFI_AUTH_WPA2_WPA3_PSK,
        };

        namespace detail
        {
            constexpr auto CONNECTED_BIT = std::uint32_t{ BIT0 };
            constexpr auto FAIL_BIT      = std::uint32_t{ BIT1 };
        } // namespace detail
    }     // namespace wifi

    template<wifi::Mode::Flags MODE> struct WiFi
    {
        using Mode               = wifi::Mode;
        using AuthenticationMode = wifi::AuthenticationMode;

        static_assert(MODE & Mode::AccessPoint || MODE & Mode::Station,
                      "MODE must be one of AccessPoint, Station or both");

        WiFi() noexcept;
        WiFi(const WiFi &)  = delete;
        WiFi(const WiFi &&) = delete;
        ~WiFi() noexcept;

    public:
        WiFi &operator=(WiFi &) = delete;
        WiFi &operator=(WiFi &&) = delete;

    public:
        void set_ssid(std::string_view ssid) noexcept;
        void set_password(std::string_view password) noexcept;
        void set_authentication(AuthenticationMode mode) noexcept;

        template<std::uint16_t MAX_AP_COUNT>
        auto scan_for_access_points(std::uint16_t &count_scanned)
            -> std::array<wifi_ap_record_t, MAX_AP_COUNT>;

        void start_access_point(std::uint8_t channel, std::uint8_t max_connections = 10) noexcept;
        void stop_access_point() noexcept;

        void connect() noexcept;
        void disconnect() noexcept;

        inline bool is_connected() const noexcept
        {
            return is_connected_;
        }

    signals:
        Signal<std::uint32_t /* ip_address */> connected_to_access_point{};
        Signal<>                               disconnected_from_access_point{};

        Signal<>                                      access_point_started{};
        Signal<std::array<std::uint8_t, 6> /* MAC */> client_connected{};
        Signal<std::array<std::uint8_t, 6> /* MAC */> client_disconnected{};

        Signal<> connection_to_access_point_failed{};

    private:
        static void event_handler(void            *callback_data,
                                  esp_event_base_t event_base,
                                  std::int32_t     event_id,
                                  void            *event_data) noexcept;

    private:
        std::uint8_t ongoing_retry_count_{ 0 };
        bool         is_connected_{ false };
        bool         is_ap_running_{ false };

        wifi_ap_config_t  config_access_point_{};
        wifi_sta_config_t config_station_{};

        EventGroupHandle_t           event_group_;
        esp_event_handler_instance_t event_handler_wifi_{ nullptr };
        esp_event_handler_instance_t event_handler_ip_{ nullptr };
    };
} // namespace elsen

#include <elsen/wifi.inl>
