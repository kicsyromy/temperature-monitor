#pragma once

#include <hedley.h>

#ifdef __clang__
#define ELSEN_DIAGNOSTIC_IGNORE_CONVERSION HEDLEY_PRAGMA(clang diagnostic ignored "-Wconversion")
#elif __GNUG__
#define ELSEN_DIAGNOSTIC_IGNORE_CONVERSION HEDLEY_PRAGMA(GCC diagnostic ignored "-Wconversion")
#else
#define ELSEN_DIAGNOSTIC_IGNORE_CONVERSION
#endif
