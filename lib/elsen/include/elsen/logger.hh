#pragma once

#include <elsen/utilities.hh>

#include <fmt/format.h>

#include <esp_log.h>

#include <array>
#include <chrono>
#include <cstdint>
#include <cstring>
#include <utility>

#define ELSEN_FIRST_ARG_(N, ...) N
#define ELSEN_FIRST_ARG(args)    ELSEN_FIRST_ARG_ args

#define ELSEN_STRINGIFY_EX(x) #x
#define ELSEN_STRINGIFY(x)    ELSEN_STRINGIFY_EX(x)

/// \defgroup logger Logger
/// \brief Logging mechanism used by Nugget to convey useful information to the users of the
/// library.
///
/// Logging in Nugget, fundamentally, is very basic, it just outputs a formatted string, prefixing
/// it with a timestamp, the app_id, the logging level, the name of the function, the name of the
/// file and the line where the log was issued from.
///
/// On all supported operating systems, based on the logging level, the color of message will be
/// altered to reflect the severity of the log.
///
/// **Note**: In order for the logging facility to function properly an instance of
///           elsen::Application must be instantiated.
///
/// It supports 5 logging levels:
///  * [TRACE](\ref ELSEN_LOG_TRACE):
///    Informational message that contains very detailed information about the inner-workings of
///    Nugget. Most of the time this type of message can be ignored.\n
///    **Example**:\n
///    ```
///    ELSEN_LOG_TRACE("elsen_app", "Formatted {}", "message");
///    ```
///    ```
///    1641620375: [elsen_app] TRACE |main| main.cc:77 > Formatted message
///    ```
///
///  * [DEBUG](\ref ELSEN_LOG_DEBUG):
///    Informational message that contains detailed information about the internal flow of Nugget.
///    This differs from TRACE by the fact that it might contain information that could be useful to
///    the users of the library when something seems broken inside Nugget or when submitting a
///    bug-report.\n
///    **Example**:\n
///    ```
///    ELSEN_LOG_DEBUG("elsen_app", "Formatted {}", "message");
///    ```
///    ```
///    1641620375: [elsen_app] DEBUG |main| main.cc:79 > Formatted message
///    ```
///
///  * [INFO](\ref ELSEN_LOG_INFO):
///    Informational message that contains useful information about the the high-level flow of
///    Nugget. These messages can be useful for debugging both consumers of Nugget as well as Nugget
///    itself.\n
///    **Example**:\n
///    ```
///    ELSEN_LOG_INFO("elsen_app", "Formatted {}", "message");
///    ```
///    ```
///    1641620375: [elsen_app] INFO  |main| main.cc:78 > Formatted message
///    ```
///
///  * [WARNING](\ref ELSEN_LOG_WARN):
///    A message that contains information about a potential error in either Nugget itself or to
///    signal a potential misuse of one of its APIs.\n
///    **Example**:\n
///    ```
///    ELSEN_LOG_WARN("elsen_app", "Formatted {}", "message");
///    ```
///    ```
///    1641620375: [elsen_app] WARN  |main| main.cc:80 > Formatted message
///    ```
///
///  * [ERROR](\ref ELSEN_LOG_ERROR):
///    A message that contains information about an error in either Nugget itself or to signal
///    a misuse of one of its APIs.\n
///    **Example**:\n
///    ```
///    ELSEN_LOG_ERROR("elsen_app", "Formatted {}", "message");
///    ```
///    ```
///    1641620375: [elsen_app] ERROR |main| main.cc:81 > Formatted message
///    ```
///

/// \ingroup logger
/// \brief Write a trace message to standard output.
///
/// \param app_id A string that identifies the application that calls the macro.
#define ELSEN_LOG_TRACE(app_id, ...)                                                      \
    do                                                                                    \
    {                                                                                     \
        static constexpr const char *function_name     = __func__;                        \
        static constexpr auto        function_name_len = sizeof(__func__);                \
                                                                                          \
        static constexpr auto construct_fmt_spec = []() {                                 \
            return elsen::Logger::construct_format_specifier<function_name_len>(          \
                ELSEN_STRINGIFY(app_id),                                                  \
                "TRACE",                                                                  \
                function_name,                                                            \
                __FILE__,                                                                 \
                ELSEN_STRINGIFY(__LINE__),                                                \
                ELSEN_FIRST_ARG((__VA_ARGS__)));                                          \
        };                                                                                \
                                                                                          \
        static constexpr auto LOGGER_MACRO_character_count = construct_fmt_spec().second; \
        elsen::Logger::trace(std::make_index_sequence<LOGGER_MACRO_character_count>{},    \
                             construct_fmt_spec,                                          \
                             __VA_ARGS__);                                                \
    } while (false)

/// \ingroup logger
/// \brief Write a debug message to standard output.
///
/// \param app_id A string that identifies the application that calls the macro.
#define ELSEN_LOG_DEBUG(app_id, ...)                                                      \
    do                                                                                    \
    {                                                                                     \
        static constexpr const char *function_name     = __func__;                        \
        static constexpr auto        function_name_len = sizeof(__func__);                \
                                                                                          \
        static constexpr auto construct_fmt_spec = []() {                                 \
            return elsen::Logger::construct_format_specifier<function_name_len>(          \
                ELSEN_STRINGIFY(app_id),                                                  \
                "DEBUG",                                                                  \
                function_name,                                                            \
                __FILE__,                                                                 \
                ELSEN_STRINGIFY(__LINE__),                                                \
                ELSEN_FIRST_ARG((__VA_ARGS__)));                                          \
        };                                                                                \
                                                                                          \
        static constexpr auto LOGGER_MACRO_character_count = construct_fmt_spec().second; \
        elsen::Logger::debug(std::make_index_sequence<LOGGER_MACRO_character_count>{},    \
                             construct_fmt_spec,                                          \
                             __VA_ARGS__);                                                \
    } while (false)

// static constexpr char mata[fmt_spec.second]{};
// f<mata>();

/// \ingroup logger
/// \brief Write a informational message to standard output.
///
/// \param app_id A string that identifies the application that calls the macro.
#define ELSEN_LOG_INFO(app_id, ...)                                                       \
    do                                                                                    \
    {                                                                                     \
        static constexpr const char *function_name     = __func__;                        \
        static constexpr auto        function_name_len = sizeof(__func__);                \
                                                                                          \
        static constexpr auto construct_fmt_spec = []() {                                 \
            return elsen::Logger::construct_format_specifier<function_name_len>(          \
                ELSEN_STRINGIFY(app_id),                                                  \
                "INFO",                                                                   \
                function_name,                                                            \
                __FILE__,                                                                 \
                ELSEN_STRINGIFY(__LINE__),                                                \
                ELSEN_FIRST_ARG((__VA_ARGS__)));                                          \
        };                                                                                \
                                                                                          \
        static constexpr auto LOGGER_MACRO_character_count = construct_fmt_spec().second; \
        elsen::Logger::info(std::make_index_sequence<LOGGER_MACRO_character_count>{},     \
                            construct_fmt_spec,                                           \
                            __VA_ARGS__);                                                 \
    } while (false)

/// \ingroup logger
/// \brief Write a warning message to standard output.
///
/// \param app_id A string that identifies the application that calls the macro.
#define ELSEN_LOG_WARN(app_id, ...)                                                       \
    do                                                                                    \
    {                                                                                     \
        static constexpr const char *function_name     = __func__;                        \
        static constexpr auto        function_name_len = sizeof(__func__);                \
                                                                                          \
        static constexpr auto construct_fmt_spec = []() {                                 \
            return elsen::Logger::construct_format_specifier<function_name_len>(          \
                ELSEN_STRINGIFY(app_id),                                                  \
                "WARN",                                                                   \
                function_name,                                                            \
                __FILE__,                                                                 \
                ELSEN_STRINGIFY(__LINE__),                                                \
                ELSEN_FIRST_ARG((__VA_ARGS__)));                                          \
        };                                                                                \
                                                                                          \
        static constexpr auto LOGGER_MACRO_character_count = construct_fmt_spec().second; \
        elsen::Logger::warn(std::make_index_sequence<LOGGER_MACRO_character_count>{},     \
                            construct_fmt_spec,                                           \
                            __VA_ARGS__);                                                 \
    } while (false)

/// \ingroup logger
/// \brief Write a error message to standard output.
///
/// \param app_id A string that identifies the application that calls the macro.
#define ELSEN_LOG_ERROR(app_id, ...)                                                      \
    do                                                                                    \
    {                                                                                     \
        static constexpr const char *function_name     = __func__;                        \
        static constexpr auto        function_name_len = sizeof(__func__);                \
                                                                                          \
        static constexpr auto construct_fmt_spec = []() {                                 \
            return elsen::Logger::construct_format_specifier<function_name_len>(          \
                ELSEN_STRINGIFY(app_id),                                                  \
                "ERROR",                                                                  \
                function_name,                                                            \
                __FILE__,                                                                 \
                ELSEN_STRINGIFY(__LINE__),                                                \
                ELSEN_FIRST_ARG((__VA_ARGS__)));                                          \
        };                                                                                \
                                                                                          \
        static constexpr auto LOGGER_MACRO_character_count = construct_fmt_spec().second; \
        elsen::Logger::error(std::make_index_sequence<LOGGER_MACRO_character_count>{},    \
                             construct_fmt_spec,                                          \
                             __VA_ARGS__);                                                \
    } while (false)

/// \cond DO_NOT_DOCUMENT

namespace elsen
{
    class Logger
    {
    public:
        template<typename SpecConstructor, typename... Args, std::size_t... i>
        static inline void trace(std::index_sequence<i...>,
                                 SpecConstructor &&sc,
                                 const char *,
                                 Args &&...args)
        {
            static constexpr auto fmt_spec = sc();
            static constexpr char format[fmt_spec.second]{ fmt_spec.first[i]... };

            print_fmt<fmt_spec.second, format>(stdout,
                                               color_grey(),
                                               color_default(),
                                               std::forward<Args>(args)...);
        }

        template<typename SpecConstructor, typename... Args, std::size_t... i>
        static inline void debug(std::index_sequence<i...>,
                                 SpecConstructor &&sc,
                                 const char *,
                                 Args &&...args)
        {
            static constexpr auto fmt_spec = sc();
            static constexpr char format[fmt_spec.second]{ fmt_spec.first[i]... };

            print_fmt<fmt_spec.second, format>(stdout,
                                               color_white(),
                                               color_default(),
                                               std::forward<Args>(args)...);
        }

        template<typename SpecConstructor, typename... Args, std::size_t... i>
        static inline void info(std::index_sequence<i...>,
                                SpecConstructor &&sc,
                                const char *,
                                Args &&...args)
        {
            static constexpr auto fmt_spec = sc();
            static constexpr char format[fmt_spec.second]{ fmt_spec.first[i]... };

            print_fmt<fmt_spec.second, format>(stdout,
                                               color_default(),
                                               color_default(),
                                               std::forward<Args>(args)...);
        }

        template<typename SpecConstructor, typename... Args, std::size_t... i>
        static inline void warn(std::index_sequence<i...>,
                                SpecConstructor &&sc,
                                const char *,
                                Args &&...args)
        {
            static constexpr auto fmt_spec = sc();
            static constexpr char format[fmt_spec.second]{ fmt_spec.first[i]... };

            print_fmt<fmt_spec.second, format>(stdout,
                                               color_yellow(),
                                               color_default(),
                                               std::forward<Args>(args)...);
        }

        template<typename SpecConstructor, typename... Args, std::size_t... i>
        static inline void error(std::index_sequence<i...>,
                                 SpecConstructor &&sc,
                                 const char *,
                                 Args &&...args)
        {
            static constexpr auto fmt_spec = sc();
            static constexpr char format[fmt_spec.second]{ fmt_spec.first[i]... };

            print_fmt<fmt_spec.second, format>(stdout,
                                               color_red(),
                                               color_default(),
                                               std::forward<Args>(args)...);
        }

        template<std::size_t function_name_size,
                 std::size_t application_id_size,
                 std::size_t log_level_size,
                 std::size_t file_path_size,
                 std::size_t line_number_size,
                 std::size_t format_specifier_size,
                 typename... Args>
        static constexpr auto construct_format_specifier(
            const char (&application_id)[application_id_size],
            const char (&log_level)[log_level_size],
            const char *function_name,
            const char (&file_path)[file_path_size],
            const char (&line)[line_number_size],
            const char (&format_specifier)[format_specifier_size]) noexcept
        {
            constexpr auto result_size =
                std::size_t{ application_id_size + log_level_size + function_name_size +
                             file_path_size + line_number_size + format_specifier_size + 16 + 1 };

            auto result   = std::pair<char[result_size], std::size_t>{};
            result.second = result_size;

            const auto file_name_offset = get_file_name_offset(file_path);

            std::size_t position     = 0;
            result.first[position++] = '{';
            result.first[position++] = '}';
            result.first[position++] = ':';
            result.first[position++] = ' ';
            result.first[position++] = '[';
            for (std::size_t i = 0; i < application_id_size - 1; ++i, ++position)
            {
                result.first[position] = application_id[i];
            }
            result.first[position++] = ']';
            result.first[position++] = ' ';
            for (std::size_t i = 0; i < log_level_size - 1; ++i, ++position)
            {
                result.first[position] = log_level[i];
            }
            result.first[position++] = ' ';
            result.first[position++] = '|';
            for (std::size_t i = 0; i < function_name_size - 1; ++i, ++position)
            {
                result.first[position] = function_name[i];
            }
            result.first[position++] = '|';
            result.first[position++] = ' ';
            for (std::size_t i = 0; i < file_path_size - file_name_offset - 1; ++i, ++position)
            {
                result.first[position] = file_path[i + file_name_offset];
            }
            result.first[position++] = ':';
            for (std::size_t i = 0; i < line_number_size - 1; ++i, ++position)
            {
                result.first[position] = line[i];
            }
            result.first[position++] = ' ';
            result.first[position++] = '>';
            result.first[position++] = ' ';

            for (std::size_t i = 0; i < format_specifier_size - 1; ++i, ++position)
            {
                result.first[position] = format_specifier[i];
            }

            result.first[position] = '\0';

            return result;
        }

    private:
        template<const char *format> constexpr static std::size_t format_len()
        {
            const char *end = format;
            for (; *end != '\0'; ++end)
                ;
            return static_cast<std::size_t>(end - format);
        }

        template<std::size_t format_size, const char *format, typename... Args>
        inline static void print_fmt(std::FILE  *f,
                                     const char *prefix,
                                     const char *suffix,
                                     Args &&...args)
        {
            const auto timestamp = std::chrono::duration_cast<std::chrono::seconds>(
                                       std::chrono::system_clock::now().time_since_epoch())
                                       .count();

            fmt::print(f, "{}", prefix);
            fmt::print(f,
                       std::string_view{ format, format_len<format>() },
                       timestamp,
                       std::forward<Args>(args)...);
            fmt::print(f, "{}\n", suffix);
        }

        template<std::size_t S>
        constexpr static std::size_t get_file_name_offset(const char (&path)[S],
                                                          size_t i = S - 1) noexcept
        {
            return (path[i] == '/' || path[i] == '\\')
                       ? i + 1
                       : (i > 0 ? get_file_name_offset(path, i - 1) : 0);
        }

        inline static const char *color_default() noexcept
        {
            return "\033[00m";
        }

        inline static const char *color_grey() noexcept
        {
            return "\033[90m";
        }

        inline static const char *color_white() noexcept
        {
            return "\033[37m";
        }

        inline static const char *color_yellow() noexcept
        {
            return "\033[33m";
        }

        inline static const char *color_red() noexcept
        {
            return "\033[31m";
        }
    };
} // namespace elsen

/// \endcond
