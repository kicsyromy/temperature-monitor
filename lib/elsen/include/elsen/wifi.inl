template<elsen::wifi::Mode::Flags MODE>
elsen::WiFi<MODE>::WiFi() noexcept
  : event_group_{ xEventGroupCreate() }
{
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        this,
                                                        &event_handler_wifi_));

    if constexpr (MODE & Mode::Station)
    {
        ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                            ESP_EVENT_ANY_ID,
                                                            &event_handler,
                                                            this,
                                                            &event_handler_ip_));
    }
}

template<elsen::wifi::Mode::Flags MODE> elsen::WiFi<MODE>::WiFi::~WiFi() noexcept
{
    disconnect();

    vEventGroupDelete(event_group_);

    if (event_handler_wifi_ != nullptr)
    {
        ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT,
                                                              ESP_EVENT_ANY_ID,
                                                              event_handler_wifi_));
    }

    if (event_handler_ip_ != nullptr)
    {
        ESP_ERROR_CHECK(
            esp_event_handler_instance_unregister(IP_EVENT, ESP_EVENT_ANY_ID, event_handler_ip_));
    }
}

template<elsen::wifi::Mode::Flags MODE>
void elsen::WiFi<MODE>::set_ssid(std::string_view ssid) noexcept
{
    if constexpr (MODE & Mode::Station)
    {
        static constexpr std::size_t MAX_SIZE = sizeof(wifi_config_t::sta.ssid) - 1;

        const std::size_t byte_count = ssid.size() > MAX_SIZE ? MAX_SIZE : ssid.size();
        std::memcpy(config_station_.ssid, ssid.data(), byte_count);
        config_station_.ssid[byte_count] = '\0';
    }

    if constexpr (MODE & Mode::AccessPoint)
    {
        static constexpr std::size_t MAX_SIZE = sizeof(wifi_config_t::ap.ssid) - 1;

        const std::size_t byte_count = ssid.size() > MAX_SIZE ? MAX_SIZE : ssid.size();
        std::memcpy(config_access_point_.ssid, ssid.data(), byte_count);
        config_access_point_.ssid[byte_count] = '\0';
    }
}

template<elsen::wifi::Mode::Flags MODE>
void elsen::WiFi<MODE>::set_password(std::string_view password) noexcept
{
    if constexpr (MODE & Mode::Station)
    {
        static constexpr std::size_t MAX_SIZE = sizeof(wifi_config_t::sta.password) - 1;

        const std::size_t byte_count = password.size() > MAX_SIZE ? MAX_SIZE : password.size();
        std::memcpy(config_station_.password, password.data(), byte_count);
        config_station_.password[byte_count] = '\0';
    }

    if constexpr (MODE & Mode::AccessPoint)
    {
        static constexpr std::size_t MAX_SIZE = sizeof(wifi_config_t::ap.password) - 1;

        const std::size_t byte_count = password.size() > MAX_SIZE ? MAX_SIZE : password.size();
        std::memcpy(config_access_point_.password, password.data(), byte_count);
        config_access_point_.password[byte_count] = '\0';
    }
}

template<elsen::wifi::Mode::Flags MODE>
void elsen::WiFi<MODE>::set_authentication(AuthenticationMode mode) noexcept
{
    if constexpr (MODE & Mode::Station)
    {
        config_station_.threshold.authmode =
            static_cast<decltype(wifi_config_t::sta.threshold.authmode)>(mode);
    }

    if constexpr (MODE & Mode::AccessPoint)
    {
        config_access_point_.authmode = static_cast<decltype(wifi_config_t::ap.authmode)>(mode);
    }
}

template<elsen::wifi::Mode::Flags MODE>
template<std::uint16_t MAX_AP_COUNT>
auto elsen::WiFi<MODE>::scan_for_access_points(std::uint16_t &count_scanned)
    -> std::array<wifi_ap_record_t, MAX_AP_COUNT>
{
    static_assert(MODE & Mode::Station);

    std::array<wifi_ap_record_t, MAX_AP_COUNT> result{};

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_start());

    esp_wifi_scan_start(nullptr, true);

    auto max_ap_count = MAX_AP_COUNT;
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&max_ap_count, result.data()));
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_num(&count_scanned));

    ESP_ERROR_CHECK(esp_wifi_stop());

    return result;
}

template<elsen::wifi::Mode::Flags MODE>
void elsen::WiFi<MODE>::start_access_point(std::uint8_t channel,
                                           std::uint8_t max_connections) noexcept
{
    static_assert(MODE & Mode::AccessPoint);

    auto config = wifi_config_t{};
    std::memcpy(&config.ap, &config_access_point_, sizeof(wifi_ap_config_t));
    config.ap.channel        = channel;
    config.ap.max_connection = max_connections;

    LOG_INFO("Starting Access Point {}, with authentication {}, password {}",
             reinterpret_cast<const char *>(config.ap.ssid),
             static_cast<int>(config.ap.authmode),
             reinterpret_cast<const char *>(config.ap.password));

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &config));
    ESP_ERROR_CHECK(esp_wifi_start());

    is_ap_running_ = true;
    access_point_started.emit();
}

template<elsen::wifi::Mode::Flags MODE> void elsen::WiFi<MODE>::stop_access_point() noexcept
{
    static_assert(MODE & Mode::AccessPoint);

    if (is_ap_running_)
    {
        ESP_ERROR_CHECK(esp_wifi_stop());
    }
}

template<elsen::wifi::Mode::Flags MODE> void elsen::WiFi<MODE>::connect() noexcept
{
    static_assert(MODE & Mode::Station);

    auto config = wifi_config_t{};
    std::memcpy(&config.sta, &config_station_, sizeof(wifi_sta_config_t));

    LOG_INFO("Starting Access Point {}, with authentication {}, password {}",
             reinterpret_cast<const char *>(config.sta.ssid),
             static_cast<int>(config.sta.threshold.authmode),
             reinterpret_cast<const char *>(config.sta.password));

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &config));
    ESP_ERROR_CHECK(esp_wifi_start());

    /* Following call blocks until one of the BITs is set */
    [[maybe_unused]] EventBits_t bits =
        xEventGroupWaitBits(event_group_,
                            wifi::detail::CONNECTED_BIT | wifi::detail::FAIL_BIT,
                            pdFALSE,
                            pdFALSE,
                            portMAX_DELAY);
}

template<elsen::wifi::Mode::Flags MODE> void elsen::WiFi<MODE>::disconnect() noexcept
{
    static_assert(MODE & Mode::Station);

    if (is_connected_)
    {
        ongoing_retry_count_ = 0;
        ESP_ERROR_CHECK(esp_wifi_stop());
    }
}

template<elsen::wifi::Mode::Flags MODE>
void elsen::WiFi<MODE>::event_handler(void            *callback_data,
                                      esp_event_base_t event_base,
                                      std::int32_t     event_id,
                                      void            *event_data) noexcept
{
    auto &self = *static_cast<WiFi *>(callback_data);

    if (event_base == WIFI_EVENT)
    {
        LOG_INFO("Got event: {}", event_id);

        if (event_id == WIFI_EVENT_STA_START)
        {
            esp_wifi_connect();
        }
        else if (event_id == WIFI_EVENT_STA_DISCONNECTED)
        {
            if (self.is_connected_)
            {
                self.is_connected_ = false;
                self.disconnected_from_access_point.emit();
            }
            else if (self.ongoing_retry_count_ < 5)
            {
                esp_wifi_connect();
                ++(self.ongoing_retry_count_);
                ELSEN_LOG_WARN(elsen,
                               "Connection failed, retrying for the {}th time",
                               self.ongoing_retry_count_);
            }
            else
            {
                xEventGroupSetBits(self.event_group_, wifi::detail::FAIL_BIT);
                self.connection_to_access_point_failed.emit();
            }
        }
        else if (event_id == WIFI_EVENT_AP_STACONNECTED)
        {
            const auto *const event = static_cast<wifi_event_ap_staconnected_t *>(event_data);
            std::array<std::uint8_t, 6> mac_address{};
            std::memcpy(mac_address.data(), event->mac, mac_address.size());

            self.client_connected.emit(mac_address);
        }
        else if (event_id == WIFI_EVENT_AP_STADISCONNECTED)
        {
            const auto *const event = static_cast<wifi_event_ap_stadisconnected_t *>(event_data);
            std::array<std::uint8_t, 6> mac_address{};
            std::memcpy(mac_address.data(), event->mac, mac_address.size());

            self.client_disconnected.emit(mac_address);
        }
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        const auto *event         = static_cast<ip_event_got_ip_t *>(event_data);
        self.ongoing_retry_count_ = 0;
        self.is_connected_        = true;
        self.connected_to_access_point.emit(event->ip_info.ip.addr);
        xEventGroupSetBits(self.event_group_, wifi::detail::CONNECTED_BIT);
    }
}
