#pragma once

#include <elsen/logger.hh>
#include <elsen/signal.hh>

#include <esp_system.h>

#include <chrono>
#include <functional>
#include <string_view>
#include <utility>

namespace elsen
{
    namespace ota
    {
        struct Config
        {
            /// URI of the OTA image (i.e. https://domain/ota_image_name.bin)
            std::string_view url;
            /// The certificate used by the server.
            /// The certificate date must be in text form with a '\0' at the end.
            const std::uint8_t *ota_server_certificate;
        };

        enum class Error
        {
            /// No error
            Ok,
            /// A connection to the OTA server has not been established
            NoConnection,
            /// Invalid configuration parameters
            InvalidArgument,
            /// Failed to read update image descriptor
            ImageInfoReadFailed,
            /// Invalid application update image
            ValidateFailed,
            /// Failed to allocate memory
            AllocationFailed,
            /// Flash write failed
            FlashError,
            /// Unknown error
            Unknown,
        };

        using TimePoint = std::chrono::time_point<std::chrono::system_clock>;
    } // namespace ota

    class OtaHandler
    {
    public:
        OtaHandler(ota::Config &&config) noexcept;

    public:
        Signal<>                                                error;
        Signal<>                                                connected;
        Signal<>                                                disconnected;
        Signal<>                                                headers_sent;
        Signal<std::string /* name */, std::string /* value */> header_received;
        Signal<std::vector<std::uint8_t> /* data */>            data_received;
        Signal<>                                                finished;

    public:
        /// Initiates a connection to the configured OTA server
        ota::Error establish_connection() noexcept;

        /// Closes the connection with the OTA server
        ota::Error close_connection();

        /// Checks if an updated image exists on the OTA server.
        /// The check is performed using SHA256 signature of the currently deployed image and the
        /// one present on the server. If they differ this function returns true otherwise returns
        /// false.
        std::pair<bool, ota::Error> is_update_available() const;

        /// Execute firmware upgrade over the air
        ota::Error execute_ota_update();

        /// The date and time that the OTA image on the server was compiled.
        std::pair<ota::TimePoint, ota::Error> update_compiled_at() const;

    private:
        std::string         ota_url_{};
        const std::uint8_t *ota_server_certificate_{};

        bool connection_established_{ false };

        void *ota_handle_{};
    };

    namespace ota
    {
        inline void try_ota_update(std::string_view url, const std::uint8_t *certificate) noexcept
        {
            auto ota_handler = OtaHandler{ { url, certificate } };

            ELSEN_LOG_INFO(elsen, "Checking for OTA update...");
            if (ota_handler.establish_connection() == ota::Error::Ok)
            {
                const auto is_update_available_result = ota_handler.is_update_available();
                const auto is_update_available        = is_update_available_result.first;
                if (!is_update_available)
                {
                    const auto error = is_update_available_result.second;
                    if (error != ota::Error::Ok)
                    {
                        ELSEN_LOG_ERROR(elsen,
                                        "Error when checking for OTA update: {}",
                                        static_cast<int>(error));
                    }

                    ELSEN_LOG_INFO(elsen, "No OTA update available...");

                    return;
                }

                ELSEN_LOG_INFO(elsen, "Starting OTA update...");
                if (auto result = ota_handler.execute_ota_update(); result != ota::Error::Ok)
                {
                    ELSEN_LOG_ERROR(elsen, "OTA update failed: {}", static_cast<int>(result));
                }
                else
                {
                    ELSEN_LOG_INFO(elsen, "Starting OTA successful, rebooting...");
                    esp_restart();
                }
            }
        }
    } // namespace ota
} // namespace elsen