#pragma once

#include <elsen/logger.hh>
#include <elsen/utilities.hh>

#include <functional>
#include <mutex>
#include <optional>
#include <type_traits>
#include <vector>

/// \defgroup signal Signal
/// \brief An implementation of the observer pattern.

#define signals public

namespace elsen
{
    namespace signal
    {
        /// \brief How to connect a signal to a handler.
        /// \ingroup signal
        enum class ConnectionType
        {
            /// The signal handler will be called from the thread that calls the
            /// [emit](\ref Signal::emit) member function on the Signal.
            Direct,
            /// The signal handler will be called from the thread that started the main application
            /// loop.
            Queued,
        };

        /// \brief A unique identifier for a connection.
        /// \ingroup signal
        using ConnectionId = std::size_t;

        void init_queued_signal_handler() noexcept;

        namespace detail
        {
            void post_signal_event(std::function<void()> *callback) noexcept;
        }
    } // namespace signal

    /// \brief Class that encapsulates an implementation of the observer pattern.
    ///
    /// \ingroup signal
    ///
    /// This class functions as an easy way of attaching an event, modeled using this class, to any
    /// arbitrary number of event handlers without creating strong coupling between classes or
    /// components.
    ///
    /// One Signal can be connected to any number of signal handlers or even to other Signals that
    /// match the template argument types.
    ///
    /// Any and all signal handlers must have their arguments be the exact same type as the template
    /// arguments for the Signal class; no implicit conversions are done when connecting or emitting
    /// a signal.
    ///
    /// Signals can be connected using either a [direct](\ref signal::ConnectionType::Direct)
    /// connection, which means that the thread on which the signal handlers are called is the same
    /// thread that emitted the signal, or a [queued](\ref signal::ConnectionType::Queued)
    /// connection where the thread on which the signal handlers are called is the same thread that
    /// the main application loop is running on.
    ///
    /// When using queued connections make sure that any objects or state used in the signal handler
    /// or sent as a part of the emit call, has an appropriate lifetime, otherwise the behaviour is
    /// undefined.
    ///
    /// Usually, but not enforced in any way, signals are used as a part of the public interface of
    /// a class, and defines a set of events that said class exposes as a part of that interface.
    ///
    /// \tparam Args The data that the Signal forwards when [emit](\ref Signal::emit) is called.
    ///
    /// **Example:**
    /// ```
    /// struct ClassWithSignal
    /// {
    /// signals:
    ///     Signal<int> number_changed;
    /// };
    ///
    /// auto s = ClassWithSignal{};
    /// s.number_changed.connect([](int new_number) {
    ///     // do something when the number changes
    /// });
    /// ...
    /// // Somewhere else in the code
    /// number_changed.emit(42); // This immediately calls the inline lambda defined above, on the
    ///                          // same thread as the one where this emit is called
    /// ```
    template<typename... Args> class Signal
    {
        using ConnectionType = signal::ConnectionType;
        using CallbackType   = void (*)(Args..., void *);

        template<typename... Ts> static auto make_subpack_tuple(Ts &&...xs)
        {
            return std::tuple<Ts...>(std::forward<Ts>(xs)...);
        }

    public:
        /// \brief Construct a new instance of a Signal.
        Signal() = default;

        /// \cond DO_NOT_DOCUMENT
        ~Signal() noexcept
        {
            [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
            for (std::size_t connection_id = 0; connection_id < connections_.size();
                 ++connection_id)
            {
                connections_[connection_id] = std::nullopt;
            }
        }

        Signal(const Signal &) = default;
        Signal &operator=(const Signal &) & = default;

        Signal(Signal &&) = default;
        Signal &operator=(Signal &&) & = default;
        /// \endcond

    public:
        /// \brief Connect the Signal to an appropriate member function handler.
        ///
        /// \tparam Slot The member function that is called when the Signal is
        /// [emit](\ref Signal::emit)ed
        /// \tparam Receiver Automatically deduced class type from the Slot
        /// \param object The instance of the class where the member function is called
        /// \param ct How should the Signal handler be called; see
        /// [ConnectionType](\ref signal::ConnectionType)
        /// \returns The unique identifier of the connection
        ///
        /// **Direct connection example:**
        /// ```
        /// struct ClassWithSignal
        /// {
        /// signals:
        ///     Signal<int> number_changed;
        /// };
        ///
        /// struct ClassWithHandler
        /// {
        ///     ClassWithSignal cws_;
        ///
        ///     ClassWithHandler()
        ///       : cws_{}
        ///     {
        ///         cws_.number_changed.connect<ClassWithHandler::on_number_changed>(*this);
        ///     }
        ///
        ///     void on_number_changed(int new_value)
        ///     {
        ///         // do something when the number changes
        ///     }
        /// };
        /// ...
        /// // Somewhere else in the code
        /// number_changed.emit(42); // This immediately calls the member function of
        ///                          // ClassWithHandler, on the same thread as the one where this
        ///                          // emit is called
        /// ```
        ///
        /// **Queued connection example:**
        /// ```
        /// struct ClassWithSignal
        /// {
        /// signals:
        ///     Signal<int> number_changed;
        /// };
        ///
        /// struct ClassWithQueuedHandler
        /// {
        ///     ClassWithSignal cws_;
        ///
        ///     ClassWithQueuedHandler()
        ///       : cws_{}
        ///     {
        ///         cws_.number_changed.connect<ClassWithHandler::on_number_changed>(
        ///             *this,
        ///             elsen::signal::ConnectionType::Queued
        ///         );
        ///     }
        ///
        ///     void on_number_changed(int new_value)
        ///     {
        ///         // do something when the number changes
        ///     }
        /// };
        /// ...
        /// // Somewhere else in the code
        /// number_changed.emit(42); // This queues up the call to
        ///                          // ClassWithQueuedHandler::on_number_changed and calls the
        ///                          // function on the thread that the main application loop is
        ///                          // running on
        /// ```
        template<auto Slot,
                 typename Receiver = typename utilities::GetClassType<decltype(Slot)>::Type>
        signal::ConnectionId connect(Receiver &object, ConnectionType ct = ConnectionType::Direct)
        {
            switch (ct)
            {
            case ConnectionType::Direct: {
                const auto cb = [](Args... args, void *context) {
                    auto *receiver = static_cast<Receiver *>(context);
                    (receiver->*Slot)(std::forward<Args>(args)...);
                };

                [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
                connections_.emplace_back(std::in_place, &object, cb);
            }
            break;

            case ConnectionType::Queued: {
                const auto cb = [](Args... args, void *context) {
                    auto arg_pack = make_subpack_tuple(std::forward<Args>(args)...);
                    signal::detail::post_signal_event(
                        new std::function<void()>{ [arg_pack = std::move(arg_pack), context] {
                            // TODO: Figure out why this is const
                            auto packed_args =
                                const_cast<std::remove_const_t<decltype(arg_pack)> *>(&arg_pack);
                            auto *receiver = static_cast<Receiver *>(context);
                            std::apply(
                                [receiver](auto &&...a) {
                                    (receiver->*Slot)(std::forward<decltype(a)>(a)...);
                                },
                                std::move(*packed_args));
                        } });
                };

                [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
                connections_.emplace_back(std::in_place, &object, cb);
            }
            break;
            }

            [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
            return connections_.size() - 1;
        }

        /// \brief Connect the Signal to an appropriate non-member function handler.
        ///
        /// This function is meant to be used for simple stateless functions.
        ///
        /// \tparam Slot The type of the non-member function that is called when the Signal is
        /// [emit](\ref Signal::emit)ed
        /// \param slot The non-member function instance that is called when the Signal is
        /// [emit](\ref Signal::emit)ed
        /// \param ct How should the Signal handler be called; see
        /// [ConnectionType](\ref signal::ConnectionType)
        /// \returns The unique identifier of the connection
        ///
        /// **Direct connection example:**
        /// ```
        /// struct ClassWithSignal
        /// {
        /// signals:
        ///     Signal<int> number_changed;
        /// };
        /// ...
        /// void on_number_changed(int new_value)
        /// {
        ///     // do something when the number changes
        /// }
        /// ...
        /// auto cws = ClassWithSignal{};
        /// cws.number_changed.connect(&on_number_changed);
        /// ...
        /// // Somewhere else in the code
        /// number_changed.emit(42); // This immediately calls the on_number_changed function, on
        ///                          // the same thread as the one where this emit is called
        /// ```
        ///
        /// **Queued connection example:**
        /// ```
        /// struct ClassWithSignal
        /// {
        /// signals:
        ///     Signal<int> number_changed;
        /// };
        /// ...
        /// void on_number_changed(int new_value)
        /// {
        ///     // do something when the number changes
        /// }
        /// ...
        /// auto cws = ClassWithSignal{};
        /// cws.number_changed.connect(&on_number_changed, elsen::signal::ConnectionType::Queued);
        /// ...
        /// // Somewhere else in the code
        /// number_changed.emit(42); // This queues up the call to on_number_changed and calls the
        ///                          // function on the thread that the main application loop is
        ///                          // running on
        /// ```
        template<typename Slot,
                 std::enable_if_t<std::is_invocable_r_v<void, Slot, Args...>, int> = 0>
        signal::ConnectionId connect(Slot &&slot, ConnectionType ct = ConnectionType::Direct)
        {
            struct Context
            {
                void (*cb)(Args...);
            };
            auto      *c           = new Context{ static_cast<void (*)(Args...)>(slot) };
            const auto ctx_deleter = [](void *obj) {
                delete static_cast<Context *>(obj);
            };

            switch (ct)
            {
            case ConnectionType::Direct: {
                const auto cb = [](Args... args, void *context) {
                    static_cast<Context *>(context)->cb(std::forward<Args>(args)...);
                };

                [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
                connections_.emplace_back(std::in_place, c, cb, ctx_deleter);
            }
            break;

            case ConnectionType::Queued: {
                const auto cb = [](Args... args, void *context) {
                    auto arg_pack = make_subpack_tuple(std::forward<Args>(args)...);
                    signal::detail::post_signal_event(
                        new std::function<void()>{ [arg_pack = std::move(arg_pack), context] {
                            auto packed_args =
                                const_cast<std::remove_const_t<decltype(arg_pack)> *>(&arg_pack);

                            auto *f = static_cast<Context *>(context)->cb;
                            std::apply(f, std::move(*packed_args));
                        } });
                };

                [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
                connections_.emplace_back(std::in_place, c, cb, ctx_deleter);
            }
            break;
            }

            [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
            return connections_.size() - 1;
        }

        /// \brief Connect the Signal to an appropriate non-member function handler.
        ///
        /// This function differs from the one above by also accepting some context/state that will
        /// be passed to all signal handlers when this signal is [emit](\ref Signal::emit)ed.
        ///
        /// Special care should be taken when using this variant of the connect function since the
        /// ownership of the context parameter is not transferred to the signal and must be alive
        /// and in a valid state as long as any signal handler has the potential to be called.
        /// If the context object is destroyed before or during a call to a signal handler the
        /// behaviour is undefined.
        ///
        /// Another potential issue with shared state is thread-safety; Signal does not make any
        /// guarantees in this regard, so if there is a potential for race-conditions, dead-locks,
        /// or other thread-related issues, synchronization must be done, as appropriate, manually,
        /// in the signal handlers.
        ///
        /// \tparam Slot The type of the non-member function that is called when the Signal is
        /// [emit](\ref Signal::emit)ed
        /// \tparam ContextType The type of the context/state that will be passed to any Signal
        /// handlers
        /// \param slot The non-member function instance that is called when the Signal is
        /// [emit](\ref Signal::emit)ed
        /// \param context The instance of the context/state data that will be passed to any handler
        /// of this Signal
        /// \param ct How should the Signal handler be called; see
        /// [ConnectionType](\ref signal::ConnectionType)
        /// \returns The unique identifier of the connection
        ///
        /// **Direct connection example:**
        /// ```
        /// struct ClassWithSignal
        /// {
        /// signals:
        ///     Signal<int> number_changed;
        /// };
        /// ...
        /// void on_number_changed(int new_value, float *state)
        /// {
        ///     // do something when the number changes
        ///     // inspect the state
        /// }
        /// auto state = float{ 3.14F }
        /// ...
        /// auto cws = ClassWithSignal{};
        /// cws.number_changed.connect(&on_number_changed, state);
        /// ...
        /// // Somewhere else in the code
        /// number_changed.emit(42); // This immediately calls the on_number_changed function, on
        ///                          // the same thread as the one where this emit is called
        /// ```
        ///
        /// **Queued connection example:**
        /// ```
        /// struct ClassWithSignal
        /// {
        /// signals:
        ///     Signal<int> number_changed;
        /// };
        /// ...
        /// void on_number_changed(int new_value, float *state)
        /// {
        ///     // do something when the number changes
        ///     // inspect the state
        /// }
        /// auto state = float{ 3.14F }
        /// ...
        /// auto cws = ClassWithSignal{};
        /// cws.number_changed.connect(
        ///     &on_number_changed,
        ///     state,
        ///     elsen::signal::ConnectionType::Queued
        /// );
        /// ...
        /// // Somewhere else in the code
        /// number_changed.emit(42); // This queues up the call to on_number_changed and calls the
        ///                          // function on the thread that the main application loop is
        ///                          // running on
        /// ```
        template<
            typename Slot,
            typename ContextType,
            std::enable_if_t<std::is_invocable_r_v<void, Slot, Args..., ContextType *>, int> = 0>
        signal::ConnectionId connect(Slot         &&slot,
                                     ContextType   &context,
                                     ConnectionType ct = ConnectionType::Direct)
        {
            struct Context
            {
                ContextType *context;
                void (*callback)(Args..., ContextType *);
            };
            auto *c = new Context{ &context, static_cast<void (*)(Args..., ContextType *)>(slot) };
            const auto ctx_deleter = [](void *obj) {
                delete static_cast<Context *>(obj);
            };

            switch (ct)
            {
            case ConnectionType::Direct: {
                const auto cb = [](Args... args, void *context_object) {
                    auto ctx = static_cast<Context *>(context_object);
                    ctx->callback(std::forward<Args>(args)..., ctx->context);
                };

                [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
                connections_.emplace_back(std::in_place, c, cb, ctx_deleter);
            }
            break;

            case ConnectionType::Queued: {
                const auto cb = [](Args... args, void *context_object) {
                    auto ctx      = static_cast<Context *>(context_object);
                    auto arg_pack = make_subpack_tuple(std::forward<Args>(args)..., ctx->context);
                    signal::detail::post_signal_event(
                        new std::function<void()>{ [arg_pack = std::move(arg_pack), ctx] {
                            auto packed_args =
                                const_cast<std::remove_const_t<decltype(arg_pack)> *>(&arg_pack);
                            std::apply(ctx->callback, std::move(*packed_args));
                        } });
                };

                [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
                connections_.emplace_back(std::in_place, c, cb, ctx_deleter);
            }
            break;
            }

            [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
            return connections_.size() - 1;
        }

        /// \brief Connect two compatible signals to each other.
        ///
        /// Signals can also be connected to other Signals in order to chain event triggers.
        /// In order for two signals to be compatible they must have the same argument types.
        ///
        /// As with regular signal handlers, when connecting to another Signal the type of the
        /// connection can be both [direct](\ref signal::ConnectionType::Direct) and
        /// [queued](\ref signal::ConnectionType::Queued).
        ///
        /// \param other The other Signal to connect this one to
        /// \param ct How should the Signal handler be called; see
        /// [ConnectionType](signal::ConnectionType)
        /// \returns The unique identifier of the connection
        ///
        /// **Example:**
        /// ```
        /// auto signal1 = Signal<int>{};
        /// auto signal2 = Signal<int>{};
        ///
        /// signal1.connect(signal2);
        /// signal2.connect(
        ///     [](int v) {
        ///         // Do something with the value
        ///     }
        /// );
        ///
        /// signal1.emit(42); // This will trigger signal1 which, in turn, will trigger signal2,
        ///                   // which, itself, will trigger the inline lambda function.
        /// ```
        signal::ConnectionId connect(Signal<Args...> &other,
                                     ConnectionType   ct = ConnectionType::Direct)
        {
            switch (ct)
            {
            case ConnectionType::Direct: {
                constexpr auto cb = [](Args... args, void *s) {
                    static_cast<Signal<Args...> *>(s)->emit(std::forward<Args>(args)...);
                };

                [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
                connections_.emplace_back(std::in_place, &other, cb);
            }
            break;

            case ConnectionType::Queued: {
                const auto cb = [](Args... args, void *context) {
                    auto arg_pack = make_subpack_tuple(std::forward<Args>(args)...);
                    signal::detail::post_signal_event(
                        new std::function<void()>{ [arg_pack = std::move(arg_pack), context] {
                            // TODO: Figure out why this is const
                            auto packed_args =
                                const_cast<std::remove_const_t<decltype(arg_pack)> *>(&arg_pack);
                            auto other_signal = static_cast<Signal<Args...> *>(context);
                            std::apply(
                                [other_signal](auto &&...a) {
                                    other_signal->emit(std::forward<decltype(a)>(a)...);
                                },
                                std::move(*packed_args));
                        } });
                };

                [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
                connections_.emplace_back(std::in_place, &other, cb);
            }
            break;
            }

            [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };
            return connections_.size() - 1;
        }

        /// \brief Remove an event handler based the connection identifier returned by a call to
        /// connect.
        void disconnect(signal::ConnectionId connection_id) noexcept
        {
            [[maybe_unused]] auto _     = std::lock_guard{ connections_mutex_ };
            connections_[connection_id] = std::nullopt;
        }

        /// \brief Trigger all signal handlers connected to this Signal.
        ///
        /// This function takes the set or arguments that shall be passed to the signal handlers.
        void emit(Args... args) const
        {
            [[maybe_unused]] auto _ = std::lock_guard{ connections_mutex_ };

            for (auto &c : connections_)
            {
                if (!c.has_value())
                {
                    continue;
                }

                auto &connection = c.value();
                connection.callback(std::forward<Args>(args)..., connection.context.get());
            }
        }

    private:
        struct Connection
        {
            inline Connection(
                void        *ctx,
                CallbackType cb,
                void (*deleter)(void *) = [](void *) {}) noexcept
              : context{ ctx, deleter }
              , callback{ cb }
            {}

            std::unique_ptr<void, void (*)(void *)> context;
            CallbackType                            callback;
        };

    private:
        std::vector<std::optional<Connection>> connections_{};
        mutable std::mutex                     connections_mutex_{};
    };
} // namespace elsen