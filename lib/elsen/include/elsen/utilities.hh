#pragma once

#include <chrono>
#include <cstdint>
#include <string_view>
#include <type_traits>

#include <FreeRTOS.h>

namespace elsen::utilities
{
    /// \cond DO_NOT_DOCUMENT
    static constexpr auto MAX_DELAY = std::chrono::milliseconds{ portMAX_DELAY };

    enum PinState
    {
        HIGH = 1,
        LOW  = 0,
    };

    enum PinMode
    {
        INPUT  = 0x00000001,
        OUTPUT = 0x00000002,
    };

    template<typename...> struct GetClassType : std::false_type
    {
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...)>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) volatile>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const volatile>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) volatile noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const volatile noexcept>
    {
        using Type = Class;
    };

    void delay(std::chrono::microseconds value) noexcept;
    void delay(std::chrono::milliseconds value) noexcept;

    void     set_pin_state(std::uint8_t pin, PinState value) noexcept;
    PinState pin_state(std::uint8_t pin) noexcept;
    void     set_pin_mode(std::uint8_t pin, PinMode value) noexcept;

    std::string_view trimmed(std::string_view in);
    /// \endcond
} // namespace elsen::utilities
