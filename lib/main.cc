#include <puma_temperature_monitor/config.hh>
#include <puma_temperature_monitor/http_server.hh>
#include <puma_temperature_monitor/io_pins.hh>
#include <puma_temperature_monitor/logger.hh>
#include <puma_temperature_monitor/status_led.hh>
#include <puma_temperature_monitor/temperature_cache.hh>
#include <puma_temperature_monitor/temperature_monitor_page.hh>
#include <puma_temperature_monitor/temperature_reader.hh>
#include <puma_temperature_monitor/trigger_config.hh>
#include <puma_temperature_monitor/triggers_page.hh>
#include <puma_temperature_monitor/wifi_config.hh>
#include <puma_temperature_monitor/wificonfig_page.hh>

#include <resources/resources.h>

#include <elsen/ota.hh>
#include <elsen/signal.hh>
#include <elsen/wifi.hh>

#include <fmt/format.h>

#include <esp_wifi.h>

#include <array>
#include <memory>

namespace
{
    template<std::size_t BUFFER_SIZE, std::size_t CACHE_SIZE>
    bool update_temperature_cache(puma_temperature_monitor::TemperatureReader<BUFFER_SIZE> &reader,
                                  puma_temperature_monitor::TemperatureCache<CACHE_SIZE>   &cache)
    {
        using SensorId = puma_temperature_monitor::SensorId;

        const auto values = reader.read_values();
        if (!values)
        {
            LOG_ERROR("Failed to read data from UART: {}", static_cast<int>(values.error()));
            return false;
        }

        const auto temperatures = values.value();
        if (temperatures.has_value())
        {
            cache.push(temperatures.value());

            LOG_INFO("A: {} - B: {} - C: {} - D: {} - E: {}",
                     cache.last()[SensorId::A],
                     cache.last()[SensorId::B],
                     cache.last()[SensorId::C],
                     cache.last()[SensorId::D],
                     cache.last()[SensorId::E]);

            return true;
        }

        return false;
    }
} // namespace

[[noreturn]] void entrypoint()
{
    using namespace elsen::utilities;
    using namespace PUMA_TEMPERATURE_MONITOR_NAMESPACE;
    using namespace std::chrono_literals;

    elsen::signal::init_queued_signal_handler();

#if PUMA_TEMPERATURE_MONITOR_ENABLE_TESTING
    [[noreturn]] extern void test_entrypoint();
    test_entrypoint();
#else
    auto status_led = StatusLed{};
    status_led.start_blinking(500ms);

    auto wifi_config = WiFiConfig::Entry{};
    auto wifi        = elsen::WiFi<elsen::wifi::Mode::Station | elsen::wifi::Mode::AccessPoint>{};

    wifi.access_point_started.connect([] { LOG_INFO("Started WiFi access point..."); });

    wifi.client_connected.connect([](const auto mac_address) {
        LOG_INFO("Client '{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}' connected",
                 MAC2STR(mac_address));
    });

    wifi.client_disconnected.connect([](const auto mac_address) {
        LOG_INFO("Client '{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}' disconnected",
                 MAC2STR(mac_address));
    });

    wifi.connected_to_access_point.connect([](const auto ip_address) {
        const auto esp_ip_addr = esp_ip4_addr_t{ ip_address };
        LOG_INFO("Connected to AP, got IP: {}.{}.{}.{}", IP2STR(&esp_ip_addr));
    });

    wifi.connection_to_access_point_failed.connect(
        [](WiFiConfig::Entry *wcfg) {
            LOG_ERROR("Failed to connect to AP with SSID {}", wcfg->ssid);
        },
        wifi_config);

    auto wifi_config_opt = WiFiConfig::load();
    if (wifi_config_opt.has_value())
    {
        wifi_config = wifi_config_opt.value();
        wifi.set_ssid(wifi_config.ssid);
        wifi.set_authentication(wifi_config.authentication_mode);
        wifi.set_password(wifi_config.password);
        wifi.connect();

        LOG_INFO("OTA: Firmware server URI {}", CONFIG_PTM_OTA_URI);
        // Let's not auto-update, we can update on-demand only for now.
        elsen::ota::try_ota_update(CONFIG_PTM_OTA_URI, resource_ota_server_cert);
    }

    if (!wifi.is_connected())
    {
        wifi.set_ssid("ESP32 AP");
        wifi.set_authentication(elsen::wifi::AuthenticationMode::Open);
        wifi.start_access_point(1);
    }

    auto short_term_cache = std::make_unique<TemperatureCache<30>>();
    auto long_term_cache  = std::make_unique<TemperatureCache<120>>();

    auto temp_reader =
        TemperatureReader<64>::create<UART_NUM_1, IO_PIN_RS485_TXD, IO_PIN_RS485_RXD>();
    auto trigger_config = TriggerConfig<3>{};

    auto http_server = HttpServer{ 80 };

    auto temp_monitor_page = TemperatureMonitorPage{ http_server, *long_term_cache };
    temp_monitor_page.register_page();

    auto triggers_page = TriggersPage{ http_server, trigger_config };
    triggers_page.register_page();

    auto wificonfig_page = WiFiConfigPage{ http_server, wifi, wifi_config };
    wificonfig_page.register_page();

    set_pin_mode(IO_PIN_CTRL_CH_A, PinMode::OUTPUT);
    set_pin_mode(IO_PIN_CTRL_CH_B, PinMode::OUTPUT);

    // Do an initial reading and fill the cache with that value
    while (!update_temperature_cache(temp_reader, *short_term_cache))
        ;
    short_term_cache->fill_with_latest();
    long_term_cache->push(short_term_cache->averages());
    long_term_cache->fill_with_latest();

    for (;;)
    {
        for (std::size_t i = 0;
             i < (std::remove_pointer_t<decltype(short_term_cache.get())>::SIZE);)
        {
            if (update_temperature_cache(temp_reader, *short_term_cache))
            {
                ++i;
            }
        }

        const auto average_temps = short_term_cache->averages();
        long_term_cache->push(average_temps);

        LOG_INFO("Temp averages: A: {} B: {} C: {} D: {} E: {}",
                 average_temps[SensorId::A],
                 average_temps[SensorId::B],
                 average_temps[SensorId::C],
                 average_temps[SensorId::D],
                 average_temps[SensorId::E]);
        for (std::size_t i = 0; i < trigger_config.size(); ++i)
        {
            // Take a copy just in case it gets modified by the user while being checked
            const auto config = trigger_config[i];
            if (config.enabled)
            {
                const auto is_active = config.active;
                const auto should_turn_on =
                    config.on_comparator(average_temps[config.channel], config.on_limit);
                const auto should_turn_off =
                    config.off_comparator(average_temps[config.channel], config.off_limit);

                LOG_INFO("S_ON: {}; S_OFF: {}; A: {}", should_turn_on, should_turn_off, is_active);

                if (should_turn_off)
                {
                    if (is_active)
                    {
                        LOG_INFO("Activating trigger {}, for temperature sensor {:c}, turning "
                                 "channel {} OFF",
                                 i + 1,
                                 'A' + config.channel,
                                 config.output + 1);
                        set_pin_state(config.output, PinState::LOW);
                        // This is intentionally triggers_config[i], config is a copy
                        trigger_config[i].active = false;
                    }
                }
                else if (should_turn_on)
                {
                    if (!is_active)
                    {
                        LOG_INFO("Activating trigger {}, for temperature sensor {:c}, turning "
                                 "channel {} ON",
                                 i + 1,
                                 'A' + config.channel,
                                 config.output + 1);
                        set_pin_state(config.output, PinState::HIGH);
                        // This is intentionally triggers_config[i], config is a copy
                        trigger_config[i].active = true;
                    }
                }
            }
        }
    }
#endif
}
