#include <elsen/logger.hh>
#include <elsen/utilities.hh>

#include <gtest/gtest.h>

[[noreturn]] void test_entrypoint()
{
    ::testing::InitGoogleTest();
    const auto result = RUN_ALL_TESTS();

    ELSEN_LOG_INFO(puma_tests, "Test execution finished with exit code: {}", result);
    for (;;)
    {
        elsen::utilities::delay(elsen::utilities::MAX_DELAY);
    }
}
