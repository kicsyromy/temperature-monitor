## Single header implementation of std::expected with functional-style extensions
include (expected.cmake)

## Open-source formatting library providing a fast and safe alternative to printf() family of functions
include (fmt.cmake)

## GSL (C++ Core Guidelines helper library)
include (GSL.cmake)

## C/C++ a header file designed to smooth over some platform-specific annoyances
include (hedley.cmake)

## C++17 library that provides static reflection for enums
include (magic_enum.cmake)


