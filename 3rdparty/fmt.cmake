set (FMT_VERSION 8.1.1)

find_project_dependency (
    NAME FMT
    NAMES fmt
    FIND_PKG_IMPORTED_TARGET fmt::fmt
    SET_PKG_CONFIG_TARGET_PROPERTIES
    SET_CMAKE_CONFIG_TARGET_PROPERTIES
)

if (NOT FMT_FOUND)
    include (FetchContent)
    FetchContent_Declare (
        FMT-${FMT_VERSION}
        URL https://github.com/fmtlib/fmt/archive/refs/tags/${FMT_VERSION}.tar.gz
        URL_HASH SHA256=3d794d3cf67633b34b2771eb9f073bde87e846e0d395d254df7b211ef1ec7346
    )

    set (FMT_DOC OFF)
    set (FMT_INSTALL OFF)
    set (FMT_OS OFF)
    set (FMT_TEST OFF)

    FetchContent_MakeAvailable (FMT-${FMT_VERSION})

    add_library (${PROJECT_ID}_fmt INTERFACE)
    add_library (${PROJECT_ID}::FMT ALIAS ${PROJECT_ID}_fmt)
    target_link_libraries (${PROJECT_ID}_fmt INTERFACE fmt::fmt)
endif ()
