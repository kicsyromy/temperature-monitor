#pragma once

#include <puma_temperature_monitor/config.hh>

#include <elsen/utilities.hh>

#include <tl/expected.hpp>

#include <driver/uart.h>

#include <array>
#include <cstdint>
#include <optional>

PUMA_TEMPERATURE_MONITOR_BEGIN_NAMESPACE

enum SensorId : std::uint8_t
{
    A = 0,
    B,
    C,
    D,
    E,
    Count,
    Unknown = 255
};

enum class UartReadError : std::uint8_t
{
    Unknown
};

template<std::size_t BUFFER_SIZE> class TemperatureReader
{
public:
    static constexpr auto SENSOR_COUNT = static_cast<std::size_t>(SensorId::Count);

public:
    ~TemperatureReader()
    {
        uart_driver_delete(uart_port_no_);
    }

public:
    template<std::uint8_t UART_NUMBER, std::uint8_t PIN_TX, std::uint8_t PIN_RX>
    static TemperatureReader create()
    {
        auto self          = TemperatureReader{};
        self.uart_port_no_ = UART_NUMBER;
        self.template set_up_uart<UART_NUMBER, PIN_TX, PIN_RX>();

        return self;
    }

    tl::expected<std::optional<std::array<float, SENSOR_COUNT>>, UartReadError> read_values()
    {
        const auto byte_count = uart_read_bytes(uart_port_no_,
                                                uart_rx_buffer.data(),
                                                BUFFER_SIZE,
                                                20 / portTICK_PERIOD_MS);

        std::array<float, SENSOR_COUNT> values{};
        if (byte_count > 0)
        {
            LOG_DEBUG("Read {} bytes from UART:", byte_count);
            auto payload = reinterpret_cast<char *>(uart_rx_buffer.data());
            LOG_DEBUG("'{}'", payload);

            auto       substr_start = std::size_t{ 0 };
            const auto separator    = ';';
            for (std::size_t i = 0; i < static_cast<std::size_t>(byte_count); ++i)
            {
                if (payload[i] == separator)
                {
                    const auto [sensor_id, temperature] =
                        parse_temperature_value(&payload[substr_start], &payload[i]);
                    values[static_cast<std::size_t>(sensor_id)] = temperature;

                    // Move the start of the next substring one position past the separator
                    substr_start = i + 1;
                }
            }

            // Deserialize the final one
            const auto [sensor_id, temperature] =
                parse_temperature_value(&payload[substr_start],
                                        &payload[static_cast<std::size_t>(byte_count)]);
            values[static_cast<std::size_t>(sensor_id)] = temperature;

            return { values };
        }

        if (byte_count == 0)
        {
            return { std::nullopt };
        }

        return tl::make_unexpected(UartReadError::Unknown);
    }

private:
    template<std::uint8_t UART_NUMBER, std::uint8_t PIN_TX, std::uint8_t PIN_RX> void set_up_uart()
    {
        using namespace elsen::utilities;
        using namespace PUMA_TEMPERATURE_MONITOR_NAMESPACE;

        set_pin_mode(IO_PIN_RS485_DE, PinMode::OUTPUT);
        set_pin_state(IO_PIN_RS485_DE, PinState::LOW);

        set_pin_mode(IO_PIN_RS485_RE, PinMode::OUTPUT);
        set_pin_state(IO_PIN_RS485_RE, PinState::LOW);

        uart_config_t uart_config{};
        uart_config.baud_rate           = 115200;
        uart_config.data_bits           = UART_DATA_8_BITS;
        uart_config.parity              = UART_PARITY_DISABLE;
        uart_config.stop_bits           = UART_STOP_BITS_1;
        uart_config.flow_ctrl           = UART_HW_FLOWCTRL_DISABLE;
        uart_config.rx_flow_ctrl_thresh = 122;

        static constexpr auto DRIVER_INTERNAL_BUFFER_SIZE = [] {
            if constexpr (BUFFER_SIZE < UART_FIFO_LEN)
            {
                return std::size_t{ UART_FIFO_LEN + 1 };
            }
            else
            {
                return BUFFER_SIZE;
            }
        }();

        // Configure UART parameters
        ESP_ERROR_CHECK(uart_param_config(UART_NUMBER, &uart_config));
        ESP_ERROR_CHECK(
            uart_set_pin(UART_NUMBER, PIN_TX, PIN_RX, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE));
        ESP_ERROR_CHECK(
            uart_driver_install(UART_NUMBER, DRIVER_INTERNAL_BUFFER_SIZE, 0, 0, nullptr, 0));
    }

    auto parse_temperature_value(char *begin, char *end)
    {
        // Ensure string terminator at the end of the value
        // (This is safe since we have one byte reserved at the end of the payload buffer)
        *end = '\0';

        // Create a view with just one of the values in the payload
        const auto substr = std::string_view{ begin, static_cast<std::size_t>(end - begin) };
        // Extract the prefix
        const auto prefix = [&] {
            const auto pfx_str = std::string_view{ substr.data(), 2 };
            if (pfx_str == "A:")
            {
                return SensorId::A;
            }
            if (pfx_str == "B:")
            {
                return SensorId::B;
            }
            if (pfx_str == "C:")
            {
                return SensorId::C;
            }
            if (pfx_str == "D:")
            {
                return SensorId::D;
            }
            if (pfx_str == "E:")
            {
                return SensorId::E;
            }
            return SensorId::Unknown;
        }();

        // Extract the actual value
        const auto value = substr.substr(2);

        // Convert the string into a floating point number
        const auto temperature = std::strtof(value.data(), nullptr);

        LOG_DEBUG("Found substring: '{}', with prefix: '{}', with "
                  "value: '{}', converted to float as: '{}'",
                  substr,
                  magic_enum::enum_name(prefix),
                  value,
                  temperature);

        return std::pair{ prefix, temperature };
    }

private:
    std::uint8_t                              uart_port_no_{ UART_NUM_MAX };
    std::array<std::uint8_t, BUFFER_SIZE + 1> uart_rx_buffer{};

private:
    TemperatureReader() = default;
};

PUMA_TEMPERATURE_MONITOR_END_NAMESPACE