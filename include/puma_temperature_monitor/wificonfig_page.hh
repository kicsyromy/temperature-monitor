#pragma once

#include <puma_temperature_monitor/config.hh>
#include <puma_temperature_monitor/http_server.hh>
#include <puma_temperature_monitor/logger.hh>
#include <puma_temperature_monitor/temperature_cache.hh>
#include <puma_temperature_monitor/trigger_config.hh>

#include <resources/resources.h>

#include <fmt/format.h>

#include <array>
#include <string>

PUMA_TEMPERATURE_MONITOR_BEGIN_NAMESPACE

class WiFiConfigPage
{
    using WiFi = elsen::WiFi<elsen::wifi::Mode::Station | elsen::wifi::Mode::AccessPoint>;

    static constexpr auto AUTH_MODES_STR = std::array{ "Open",     "WEP",          "",
                                                       "WPA2 PSK", "WPA/WPA2 PSK", "",
                                                       "WPA3 PSK", "WPA2/WPA3 PSK" };

    static constexpr auto AUTH_MODES_STR_ESCAPED =
        std::array{ "Open",           "WEP", "",         "WPA2+PSK",
                    "WPA%2FWPA2+PSK", "",    "WPA3+PSK", "WPA2%2FWPA3+PSK" };

    static_assert(AUTH_MODES_STR.size() == AUTH_MODES_STR_ESCAPED.size());

public:
    WiFiConfigPage(HttpServer &server, WiFi &wifi, WiFiConfig::Entry &wifi_config)
      : http_server_{ server }
      , wifi_{ wifi }
      , wifi_config_{ wifi_config }
    {}

public:
    void register_page()
    {
        http_server_.configure_uri_handler(
            "/wificonfig.html",
            HttpServer::Method::Get,
            [this](auto *request) { return handle_http_get(request); });

        http_server_.configure_uri_handler(
            "/save_wifi_config",
            HttpServer::Method::Post,
            [this](auto *request) { return handle_save_triggers(request); });

        http_server_.configure_uri_handler("/reboot", HttpServer::Method::Get, [](auto *) {
            std::exit(EXIT_SUCCESS);
            return ESP_OK;
        });
    }

private:
    bool handle_http_get(httpd_req_t *request)
    {
        static constexpr auto HTML_HEADER =
            std::string_view{ "<!DOCTYPE html><html lang=\"en\">\n" };
        static constexpr auto HTML_FOOTER = std::string_view{ "</html>" };

        static constexpr auto SCRIPT_BEGIN = std::string_view{ "<script>window.onload=()=>{\n" };
        static constexpr auto SCRIPT_END   = std::string_view{ "\n}</script>" };

        static constexpr auto HTML_SELECT_INITIALIZERS =
            std::string_view{ "let ssid=document.getElementsByName(\"ssid\").item(0);\n"
                              "let auth=document.getElementsByName(\"auth\").item(0);\n"

            };

        // Send a proper HTML header
        httpd_resp_send_chunk(request, HTML_HEADER.data(), HTML_HEADER.size());

        // Begin declaring the array of values
        httpd_resp_send_chunk(request, SCRIPT_BEGIN.data(), SCRIPT_BEGIN.size());
        httpd_resp_send_chunk(request,
                              HTML_SELECT_INITIALIZERS.data(),
                              HTML_SELECT_INITIALIZERS.size());

        auto wifi_config_js_buffer = std::vector<char>{};
        wifi_config_js_buffer.resize(60, '\0');

        auto       character_count     = std::size_t{};
        const auto auth_index          = static_cast<int>(wifi_config_.authentication_mode);
        const auto auth_selected_index = auth_index >= 5   ? auth_index - 2
                                         : auth_index >= 2 ? auth_index - 1
                                                           : auth_index;
        do
        {
            const auto count_would_be_written =
                std::snprintf(wifi_config_js_buffer.data(),
                              wifi_config_js_buffer.size(),
                              "ssid.value=\"%s\";auth.selectedIndex=%d;\n",
                              wifi_config_.ssid.data(),
                              auth_selected_index);

            if (count_would_be_written < 0)
            {
                const auto written = std::snprintf(wifi_config_js_buffer.data(),
                                                   wifi_config_js_buffer.size(),
                                                   "ssid.value=\"\";auth.selectedIndex=0;\n");
                character_count    = static_cast<std::size_t>(written);
                LOG_ERROR("Failed to serialize WiFi config values");
            }
            else if (static_cast<std::size_t>(count_would_be_written + 1) >
                     wifi_config_js_buffer.size())
            {
                LOG_WARN("Buffer size insufficient, {} vs {}, resizing...",
                         count_would_be_written + 1,
                         wifi_config_js_buffer.size());

                // Not enough room to process the string, make some more room
                wifi_config_js_buffer.resize(wifi_config_js_buffer.size() + 10, '\0');
                continue;
            }
            else
            {
                character_count = static_cast<std::size_t>(count_would_be_written);
            }

            break;
        } while (true);

        const auto trigger_setting_js =
            std::string_view{ wifi_config_js_buffer.data(), character_count };
        httpd_resp_send_chunk(request,
                              trigger_setting_js.data(),
                              static_cast<ssize_t>(trigger_setting_js.size()));

        // End the declaration of the temperature values
        httpd_resp_send_chunk(request, SCRIPT_END.data(), SCRIPT_END.size());

        // Send off the main body of the html document
        const auto html_file =
            std::string_view{ reinterpret_cast<const char *>(resource_wificonfig),
                              resource_wificonfig_size };
        httpd_resp_send_chunk(request, html_file.data(), static_cast<ssize_t>(html_file.size()));

        // Close the <html> tag
        httpd_resp_send_chunk(request, HTML_FOOTER.data(), HTML_FOOTER.size());

        // Mark the response as completed
        httpd_resp_send_chunk(request, "", 0);

        return true;
    }

    bool handle_save_triggers(httpd_req_t *request)
    {
        auto revc_buffer = std::array<char, 100>{};
        auto remaining   = request->content_len;
        auto ret         = int{};

        auto body = std::string{};

        while (remaining > 0)
        {
            const auto read_count = std::min<std::size_t>(remaining, sizeof(revc_buffer));

            /* Read the data for the request */
            if ((ret = httpd_req_recv(request, revc_buffer.data(), read_count)) < 0)
            {
                if (ret == HTTPD_SOCK_ERR_TIMEOUT)
                {
                    /* Retry receiving if timeout occurred */
                    continue;
                }
                LOG_ERROR("Failed with error code: {}", ret);
                return false;
            }

            body += std::string_view{ revc_buffer.data(), static_cast<std::size_t>(ret) };
            remaining -= static_cast<std::size_t>(ret);
        }

        body += '&';
        LOG_INFO("Body was: {}", body);
        const auto parse_error = parse_post_body(body);

        static constexpr auto RESPONSE_FMT = std::string_view{
            "<!DOCTYPE html>\n"
            "<html>\n"
            "  <head>\n"
            "    <meta http-equiv=\"refresh\" content=\"10; url='http://192.168.4.1/{}'\" />\n"
            "  </head>\n"
            "  <body>\n"
            "    <h1>{}</h1>\n"
            "    <h3>{}</h3>\n"
            "  </body>\n"
            "</html>"
        };
        const auto response = [&] {
            if (!parse_error)
            {
                LOG_INFO("Wifi config OK, saving and rebooting...");

                WiFiConfig::save(wifi_config_);
                return fmt::format(RESPONSE_FMT,
                                   "reboot",
                                   "Success!",
                                   "The device will reboot in 10 seconds...");
            }

            return fmt::format(RESPONSE_FMT, "wificonfig.html", parse_error.value(), "");
        }();

        // End response
        httpd_resp_send(request, response.data(), static_cast<ssize_t>(response.size()));

        return true;
    }

    std::optional<std::string> parse_post_body(std::string_view content)
    {
        auto result = WiFiConfig::Entry{};

        std::size_t element_start  = 0;
        std::size_t equalsPosition = std::string::npos;
        for (std::size_t i = 1; i < content.size(); ++i)
        {
            if (content[i] == '=')
            {
                equalsPosition = i;
            }

            if (content[i] == '&')
            {
                if (equalsPosition == std::string::npos)
                {
                    return fmt::format("Failed to parse element at index: {}", element_start);
                }
                else
                {
                    [[maybe_unused]] const auto element =
                        std::string_view{ &content[element_start], i - element_start };
                    const auto key =
                        std::string_view{ &content[element_start], equalsPosition - element_start };
                    const auto value =
                        std::string_view{ &content[equalsPosition + 1], i - equalsPosition - 1 };

                    if (key.size() != 4)
                    {
                        return fmt::format("Unknown setting: {}", key);
                    }
                    else
                    {
                        if (key == "ssid")
                        {
                            result.ssid = value;
                        }
                        else if (key == "auth")
                        {
                            for (std::size_t j = 0; j < AUTH_MODES_STR_ESCAPED.size(); ++j)
                            {
                                if (value == AUTH_MODES_STR_ESCAPED[j])
                                {
                                    result.authentication_mode =
                                        static_cast<elsen::wifi::AuthenticationMode>(j);
                                    break;
                                }
                            }
                        }
                        else if (key == "pass")
                        {
                            if (!value.empty())
                            {
                                result.password = value;
                            }
                            else
                            {
                                result.password = std::move(wifi_config_.password);
                            }
                        }
                        else
                        {
                            return fmt::format("Unknown setting '{}'", key);
                        }
                    }
                }

                element_start  = i + 1;
                equalsPosition = std::string::npos;
            }
        }

        LOG_INFO("\n\tSSID: {}\n\tAuthentication: {}\n\tPassword: {}",
                 result.ssid,
                 static_cast<int>(result.authentication_mode),
                 result.password);

        wifi_config_.ssid                = std::move(result.ssid);
        wifi_config_.authentication_mode = result.authentication_mode;
        wifi_config_.password            = std::move(result.password);

        return std::nullopt;
    }

private:
    HttpServer        &http_server_;
    WiFi              &wifi_;
    WiFiConfig::Entry &wifi_config_;
};

PUMA_TEMPERATURE_MONITOR_END_NAMESPACE