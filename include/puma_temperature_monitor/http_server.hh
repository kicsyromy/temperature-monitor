#pragma once

#include <puma_temperature_monitor/config.hh>
#include <puma_temperature_monitor/logger.hh>

#include <elsen/utilities.hh>

#include <esp_http_server.h>

#include <cstdint>
#include <functional>
#include <memory>

PUMA_TEMPERATURE_MONITOR_BEGIN_NAMESPACE

class HttpServer
{
public:
    enum class Method
    {
        Get  = 1,
        Post = 3,
    };

public:
    using RequestHandlerCallback = std::function<bool(httpd_req_t *)>;

public:
    HttpServer(std::uint16_t port = 80)
    {
        static_assert(static_cast<int>(Method::Get) == HTTP_GET);
        static_assert(static_cast<int>(Method::Post) == HTTP_POST);

        httpd_config_t config   = HTTPD_DEFAULT_CONFIG();
        config.server_port      = port;
        config.lru_purge_enable = true;
        config.global_user_ctx  = this;

        LOG_INFO("Starting HTTP server on port {}...", config.server_port);
        if (httpd_start(&http_server_, &config) == ESP_OK)
        {
            LOG_INFO("Started HTTP server");
            return;
        }

        LOG_ERROR("Failed to HTTP server...");
        for (;;)
        {
            elsen::utilities::delay(elsen::utilities::MAX_DELAY);
        }
    }

    void configure_uri_handler(const char            *uri,
                               Method                 method,
                               RequestHandlerCallback request_handler)
    {
        const auto callback_handler_index = request_handler_callbacks_.size();
        request_handler_callbacks_.push_back(std::move(request_handler));

        auto uri_handler     = std::make_unique<httpd_uri_t>();
        uri_handler->uri     = uri;
        uri_handler->method  = static_cast<httpd_method_t>(method);
        uri_handler->handler = [](httpd_req_t *request) -> esp_err_t {
            auto *self     = static_cast<HttpServer *>(httpd_get_global_user_ctx(request->handle));
            auto &callback = self->request_handler_callbacks_[static_cast<std::size_t>(
                reinterpret_cast<std::uint64_t>(request->user_ctx))];

            if (callback(request))
            {
                return ESP_OK;
            }

            return ESP_FAIL;
        };
        uri_handler->user_ctx =
            reinterpret_cast<void *>(static_cast<std::uint64_t>(callback_handler_index));

        httpd_register_uri_handler(http_server_, uri_handler.get());

        request_handlers_.push_back(std::move(uri_handler));
    }

private:
    httpd_handle_t                            http_server_{};
    std::vector<std::unique_ptr<httpd_uri_t>> request_handlers_{};
    std::vector<RequestHandlerCallback>       request_handler_callbacks_{};
};

PUMA_TEMPERATURE_MONITOR_END_NAMESPACE