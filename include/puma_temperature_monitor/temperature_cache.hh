#pragma once

#include <puma_temperature_monitor/config.hh>
#include <puma_temperature_monitor/temperature_reader.hh>

#include <algorithm>
#include <array>
#include <numeric>

PUMA_TEMPERATURE_MONITOR_BEGIN_NAMESPACE

template<std::size_t CACHE_SIZE = 60, typename FloatType = float> struct TemperatureCache
{
    static constexpr auto SENSOR_COUNT = TemperatureReader<0>::SENSOR_COUNT;
    static constexpr auto SIZE         = CACHE_SIZE;

    constexpr void push(const std::array<FloatType, SENSOR_COUNT> &value) noexcept
    {
        // Make room for the latest reading
        for (std::size_t i = 1; i < buffer.size(); ++i)
        {
            buffer[i - 1] = buffer[i];
        }
        buffer[CACHE_SIZE - 1] = value;
    }

    constexpr auto fill_with_latest() noexcept
    {
        for (std::size_t i = 1; i < buffer.size(); ++i)
        {
            buffer[i - 1] = buffer[CACHE_SIZE - 1];
        }
    }

    constexpr auto averages() noexcept
    {
        auto result = std::array<FloatType, SENSOR_COUNT>{};

        for (std::size_t i = 0; i < SENSOR_COUNT; ++i)
        {
            for (std::size_t j = 0; j < CACHE_SIZE; ++j)
            {
                result[i] += buffer[j][i];
            }
            result[i] /= CACHE_SIZE;
        }

        return result;
    }

    constexpr const auto &first() const noexcept
    {
        return buffer[0];
    }

    constexpr const auto &last() const noexcept
    {
        return buffer[buffer.size() - 1];
    }

    constexpr const auto *begin() const noexcept
    {
        return buffer.begin();
    }

    constexpr const auto *end() const noexcept
    {
        return buffer.end();
    }

    constexpr auto &operator[](std::size_t index) noexcept
    {
        return buffer[index];
    }

    constexpr const auto &operator[](std::size_t index) const noexcept
    {
        return buffer[index];
    }

    std::array<std::array<FloatType, SENSOR_COUNT>, CACHE_SIZE> buffer{};
};

PUMA_TEMPERATURE_MONITOR_END_NAMESPACE