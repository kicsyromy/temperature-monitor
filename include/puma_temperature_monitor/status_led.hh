#pragma once

#include <puma_temperature_monitor/config.hh>

#include <elsen/utilities.hh>

#include <esp_timer.h>

#include <chrono>

PUMA_TEMPERATURE_MONITOR_BEGIN_NAMESPACE

class StatusLed
{
public:
    StatusLed(bool initial_led_state = true)
      : led_state_{ initial_led_state }
    {
        using namespace elsen::utilities;

        auto led_state = true;
        set_pin_mode(IO_PIN_CTRL_LED, PinMode::OUTPUT);
        set_pin_state(IO_PIN_CTRL_LED, static_cast<PinState>(led_state));

        set_up_timer(
            [](void *led_state_ptr) {
                auto &ls = *static_cast<bool *>(led_state_ptr);

                ls = !ls;
                set_pin_state(IO_PIN_CTRL_LED, static_cast<PinState>(ls));
            },
            &led_state_,
            timer_);
    }

    ~StatusLed()
    {
        esp_timer_delete(timer_);
    }

public:
    void start_blinking(std::chrono::milliseconds interval)
    {
        esp_timer_start_periodic(
            timer_,
            static_cast<uint64_t>(
                std::chrono::duration_cast<std::chrono::microseconds>(interval).count()));
    }

    [[maybe_unused]] void stop_blinking()
    {
        esp_timer_stop(timer_);
    }

private:
    inline void set_up_timer(void (*callback)(void *),
                             void               *callback_argument,
                             esp_timer_handle_t &timer) noexcept
    {
        auto timer_config                  = esp_timer_create_args_t{};
        timer_config.callback              = callback;
        timer_config.arg                   = callback_argument;
        timer_config.dispatch_method       = ESP_TIMER_ISR;
        timer_config.name                  = "LED_blinker";
        timer_config.skip_unhandled_events = true;

        esp_timer_create(&timer_config, &timer);
    }

private:
    bool               led_state_{ false };
    esp_timer_handle_t timer_{};
};

PUMA_TEMPERATURE_MONITOR_END_NAMESPACE
