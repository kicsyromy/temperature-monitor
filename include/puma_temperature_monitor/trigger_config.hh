#pragma once

#include <puma_temperature_monitor/config.hh>

#include <fmt/format.h>

#include <nvs.h>
#include <nvs_flash.h>

#include <array>
#include <type_traits>

#undef ESP_ERROR_CHECK
#define ESP_ERROR_CHECK(x)                                               \
    do                                                                   \
    {                                                                    \
        if (x != ESP_OK)                                                 \
        {                                                                \
            LOG_ERROR("Failed to interact with NVS, error code: {}", x); \
        }                                                                \
    } while (false)

PUMA_TEMPERATURE_MONITOR_BEGIN_NAMESPACE

struct TriggerConfigEntry
{
    using Comparator = bool (*)(float, float);

    static constexpr Comparator lt = [](float a, float b) -> bool {
        return a < b;
    };
    static constexpr Comparator gt = [](float a, float b) -> bool {
        return a > b;
    };

    SensorId     channel;
    std::uint8_t output;
    Comparator   on_comparator;
    float        on_limit;
    Comparator   off_comparator;
    float        off_limit;
    bool         enabled;
    bool         active;
};

template<std::size_t TRIGGER_COUNT = 3> struct TriggerConfig
{
    static constexpr auto NVS_NAMESPACE_FMT      = "trigg_{}";
    static constexpr auto NVS_KEY_CHANNEL        = "channel";
    static constexpr auto NVS_KEY_OUTPUT         = "output";
    static constexpr auto NVS_KEY_ON_COMPARATOR  = "on_comparator";
    static constexpr auto NVS_KEY_ON_LIMIT       = "on_limit";
    static constexpr auto NVS_KEY_OFF_COMPARATOR = "off_comparator";
    static constexpr auto NVS_KEY_OFF_LIMIT      = "off_limit";
    static constexpr auto NVS_KEY_ENABLED        = "enabled";

    static constexpr auto TRIGGER_CONFIG_ENTRY_DEFAULT = TriggerConfigEntry{ SensorId::A,
                                                                             IO_PIN_CTRL_CH_A,
                                                                             TriggerConfigEntry::lt,
                                                                             30.F,
                                                                             TriggerConfigEntry::gt,
                                                                             40.F,
                                                                             false,
                                                                             false };

    TriggerConfig()
    {
        auto nvs_handle = nvs_handle_t{};

        for (std::size_t i = 0; i < TRIGGER_COUNT; ++i)
        {
            data[i] = TRIGGER_CONFIG_ENTRY_DEFAULT;

            const auto nvs_namespace = fmt::format(NVS_NAMESPACE_FMT, i);

            // Try and open the nvs, in read-only mode, for a specific namespace, if the namespace
            // is missing open it in read-write mode and commit the new namespace
            auto open_result = nvs_open(nvs_namespace.data(), NVS_READONLY, &nvs_handle);
            if (open_result == ESP_ERR_NVS_NOT_FOUND)
            {
                nvs_handle  = 0;
                open_result = nvs_open(nvs_namespace.data(), NVS_READWRITE, &nvs_handle);
                if (open_result == ESP_OK)
                {
                    nvs_commit(nvs_handle);
                }
            }
            ESP_ERROR_CHECK(open_result);

            {
                std::uint8_t channel;
                const auto   err = nvs_get_u8(nvs_handle, NVS_KEY_CHANNEL, &channel);
                if (err != ESP_ERR_NVS_NOT_FOUND)
                {
                    ESP_ERROR_CHECK(err);
                    data[i].channel = static_cast<SensorId>(channel);
                }
            }

            {
                std::uint8_t output;
                const auto   err = nvs_get_u8(nvs_handle, NVS_KEY_OUTPUT, &output);
                if (err != ESP_ERR_NVS_NOT_FOUND)
                {
                    ESP_ERROR_CHECK(err);
                    data[i].output = output;
                }
            }

            {
                std::uint8_t on_comparator;
                const auto   err = nvs_get_u8(nvs_handle, NVS_KEY_ON_COMPARATOR, &on_comparator);
                if (err != ESP_ERR_NVS_NOT_FOUND)
                {
                    ESP_ERROR_CHECK(err);
                    data[i].on_comparator =
                        on_comparator == 0 ? TriggerConfigEntry::lt : TriggerConfigEntry::gt;
                }
            }

            {
                std::int16_t on_limit;
                const auto   err = nvs_get_i16(nvs_handle, NVS_KEY_ON_LIMIT, &on_limit);
                if (err != ESP_ERR_NVS_NOT_FOUND)
                {
                    ESP_ERROR_CHECK(err);
                    data[i].on_limit = static_cast<float>(on_limit) / 100.F;
                }
            }

            {
                std::uint8_t off_comparator;
                const auto   err = nvs_get_u8(nvs_handle, NVS_KEY_OFF_COMPARATOR, &off_comparator);
                if (err != ESP_ERR_NVS_NOT_FOUND)
                {
                    ESP_ERROR_CHECK(err);
                    data[i].off_comparator =
                        off_comparator == 0 ? TriggerConfigEntry::lt : TriggerConfigEntry::gt;
                }
            }

            {
                std::int16_t off_limit;
                const auto   err = nvs_get_i16(nvs_handle, NVS_KEY_OFF_LIMIT, &off_limit);
                if (err != ESP_ERR_NVS_NOT_FOUND)
                {
                    ESP_ERROR_CHECK(err);
                    data[i].off_limit = static_cast<float>(off_limit) / 100.F;
                }
            }

            {
                std::uint8_t enabled;
                const auto   err = nvs_get_u8(nvs_handle, NVS_KEY_ENABLED, &enabled);
                if (err != ESP_ERR_NVS_NOT_FOUND)
                {
                    ESP_ERROR_CHECK(err);
                    data[i].enabled = static_cast<bool>(enabled);
                }
            }

            nvs_close(nvs_handle);
            nvs_handle = 0;
        }
    }

    void save()
    {
        auto nvs_handle = nvs_handle_t{};

        for (std::size_t i = 0; i < TRIGGER_COUNT; ++i)
        {
            const auto nvs_namespace = fmt::format(NVS_NAMESPACE_FMT, i);
            ESP_ERROR_CHECK(nvs_open(nvs_namespace.data(), NVS_READWRITE, &nvs_handle));
            ESP_ERROR_CHECK(nvs_set_u8(nvs_handle, NVS_KEY_CHANNEL, data[i].channel));
            ESP_ERROR_CHECK(nvs_set_u8(nvs_handle, NVS_KEY_OUTPUT, data[i].output));
            ESP_ERROR_CHECK(nvs_set_u8(nvs_handle,
                                       NVS_KEY_ON_COMPARATOR,
                                       data[i].on_comparator == TriggerConfigEntry::lt ? 0 : 1));
            ESP_ERROR_CHECK(nvs_set_i16(nvs_handle,
                                        NVS_KEY_ON_LIMIT,
                                        static_cast<std::int16_t>(data[i].on_limit * 100.F)));
            ESP_ERROR_CHECK(nvs_set_u8(nvs_handle,
                                       NVS_KEY_OFF_COMPARATOR,
                                       data[i].off_comparator == TriggerConfigEntry::lt ? 0 : 1));
            ESP_ERROR_CHECK(nvs_set_i16(nvs_handle,
                                        NVS_KEY_OFF_LIMIT,
                                        static_cast<std::int16_t>(data[i].off_limit * 100.F)));
            ESP_ERROR_CHECK(nvs_set_u8(nvs_handle,
                                       NVS_KEY_ENABLED,
                                       static_cast<std::uint8_t>(data[i].enabled)));

            nvs_commit(nvs_handle);
            nvs_close(nvs_handle);
            nvs_handle = 0;
        }
    }

    constexpr auto size() const noexcept
    {
        return data.size();
    }

    constexpr const auto *begin() const noexcept
    {
        return data.begin();
    }

    constexpr const auto *end() const noexcept
    {
        return data.end();
    }

    constexpr auto &operator[](std::size_t index) noexcept
    {
        return data[index];
    }

    constexpr const auto &operator[](std::size_t index) const noexcept
    {
        return data[index];
    }

    std::array<TriggerConfigEntry, TRIGGER_COUNT> data{};
};

PUMA_TEMPERATURE_MONITOR_END_NAMESPACE