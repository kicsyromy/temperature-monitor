#include <elsen/logger.hh>

#if defined(PUMA_LOG_EVERYTHING) || CONFIG_LOG_DEFAULT_LEVEL >= 5
#define LOG_TRACE(...) ELSEN_LOG_TRACE(puma_temp_monitor, __VA_ARGS__)
#else
#define LOG_TRACE(...)
#endif

#if defined(PUMA_LOG_EVERYTHING) || CONFIG_LOG_DEFAULT_LEVEL >= 4
#define LOG_DEBUG(...) ELSEN_LOG_DEBUG(puma_temp_monitor, __VA_ARGS__)
#else
#define LOG_DEBUG(...)
#endif

#if defined(PUMA_LOG_EVERYTHING) || CONFIG_LOG_DEFAULT_LEVEL >= 3
#define LOG_INFO(...) ELSEN_LOG_INFO(puma_temp_monitor, __VA_ARGS__)
#else
#define LOG_INFO(...)
#endif

#if defined(PUMA_LOG_EVERYTHING) || CONFIG_LOG_DEFAULT_LEVEL >= 2
#define LOG_WARN(...) ELSEN_LOG_WARN(puma_temp_monitor, __VA_ARGS__)
#else
#define LOG_WARN(...)
#endif

#if defined(PUMA_LOG_EVERYTHING) || CONFIG_LOG_DEFAULT_LEVEL >= 1
#define LOG_ERROR(...) ELSEN_LOG_ERROR(puma_temp_monitor, __VA_ARGS__)
#else
#define LOG_ERROR(...)
#endif
