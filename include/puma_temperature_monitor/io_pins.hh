#pragma once

#include <puma_temperature_monitor/config.hh>

#include <cstdint>

PUMA_TEMPERATURE_MONITOR_BEGIN_NAMESPACE

static constexpr auto IO_PIN_RS485_DE  = std::uint8_t{ 2 };  /* Output Enable [Active HIGH] */
static constexpr auto IO_PIN_RS485_RE  = std::uint8_t{ 4 };  /* Read Enable [Active LOW] */
static constexpr auto IO_PIN_CTRL_CH_A = std::uint8_t{ 12 }; /* Relay, Channel A */
static constexpr auto IO_PIN_CTRL_CH_B = std::uint8_t{ 13 }; /* Relay, Channel B */
static constexpr auto IO_PIN_CTRL_LED  = std::uint8_t{ 25 }; /* Status LED [Active HIGH] */
static constexpr auto IO_PIN_RS485_RXD = std::uint8_t{ 26 }; /* UART RX Pin */
static constexpr auto IO_PIN_RS485_TXD = std::uint8_t{ 27 }; /* UART TX Pin */

PUMA_TEMPERATURE_MONITOR_END_NAMESPACE