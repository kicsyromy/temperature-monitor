#pragma once

#include <puma_temperature_monitor/config.hh>
#include <puma_temperature_monitor/http_server.hh>
#include <puma_temperature_monitor/logger.hh>
#include <puma_temperature_monitor/temperature_cache.hh>
#include <puma_temperature_monitor/trigger_config.hh>

#include <resources/resources.h>

#include <fmt/format.h>

#include <string>

PUMA_TEMPERATURE_MONITOR_BEGIN_NAMESPACE

template<std::size_t TRIGGER_COUNT> class TriggersPage
{
public:
    TriggersPage(HttpServer &server, TriggerConfig<TRIGGER_COUNT> &trigger_config)
      : http_server_{ server }
      , trigger_config_{ trigger_config }
    {}

public:
    void register_page()
    {
        http_server_.configure_uri_handler(
            "/triggers.html",
            HttpServer::Method::Get,
            [this](auto *request) { return handle_http_get(request); });

        http_server_.configure_uri_handler(
            "/save_triggers",
            HttpServer::Method::Post,
            [this](auto *request) { return handle_save_triggers(request); });
    }

private:
    bool handle_http_get(httpd_req_t *request)
    {
        static constexpr auto HTML_HEADER =
            std::string_view{ "<!DOCTYPE html><html lang=\"en\">\n" };
        static constexpr auto HTML_FOOTER = std::string_view{ "</html>" };

        static constexpr auto SCRIPT_BEGIN = std::string_view{ "<script>window.onload=()=>{\n" };
        static constexpr auto SCRIPT_END   = std::string_view{ "\n}</script>" };

        static constexpr auto HTML_SELECT_INITIALIZERS = std::string_view{
            "let t1ch=document.getElementsByName(\"t1ch\").item(0);\n"
            "let t1out=document.getElementsByName(\"t1out\").item(0);\n"
            "let t1thontype=document.getElementsByName(\"t1thontype\").item(0);\n"
            "let t1thon=document.getElementsByName(\"t1thon\").item(0);\n"
            "let t1thofftype=document.getElementsByName(\"t1thofftype\").item(0);\n"
            "let t1thoff=document.getElementsByName(\"t1thoff\").item(0);\n"
            "let t1en=document.getElementsByName(\"t1en\").item(0);\n"
            "let t2ch=document.getElementsByName(\"t2ch\").item(0);\n"
            "let t2out=document.getElementsByName(\"t2out\").item(0);\n"
            "let t2thontype=document.getElementsByName(\"t2thontype\").item(0);\n"
            "let t2thon=document.getElementsByName(\"t2thon\").item(0);\n"
            "let t2thofftype=document.getElementsByName(\"t2thofftype\").item(0);\n"
            "let t2thoff=document.getElementsByName(\"t2thoff\").item(0);\n"
            "let t2en=document.getElementsByName(\"t2en\").item(0);\n"
            "let t3ch=document.getElementsByName(\"t3ch\").item(0);\n"
            "let t3out=document.getElementsByName(\"t3out\").item(0);\n"
            "let t3thontype=document.getElementsByName(\"t3thontype\").item(0);\n"
            "let t3thon=document.getElementsByName(\"t3thon\").item(0);\n"
            "let t3thofftype=document.getElementsByName(\"t3thofftype\").item(0);\n"
            "let t3thoff=document.getElementsByName(\"t3thoff\").item(0);\n"
            "let t3en=document.getElementsByName(\"t3en\").item(0);\n"
        };

        // Send a proper HTML header
        httpd_resp_send_chunk(request, HTML_HEADER.data(), HTML_HEADER.size());

        // Begin declaring the array of values
        httpd_resp_send_chunk(request, SCRIPT_BEGIN.data(), SCRIPT_BEGIN.size());
        httpd_resp_send_chunk(request,
                              HTML_SELECT_INITIALIZERS.data(),
                              HTML_SELECT_INITIALIZERS.size());

        auto temperature_entry_buffer = std::vector<char>{};
        temperature_entry_buffer.resize(150, '\0');
        for (std::size_t i = 0; i < trigger_config_.size(); ++i)
        {
            const auto &config     = trigger_config_[i];
            const auto  trigger_id = static_cast<int>(i + 1);

            auto character_count = std::size_t{};
            do
            {
                const auto count_would_be_written = std::snprintf(
                    temperature_entry_buffer.data(),
                    temperature_entry_buffer.size(),
                    "t%dch.value=\"%c\";t%dout.value=\"%d\";t%dthontype.value=\"%.2s\";"
                    "t%dthon.value=\"%.2f\";t%dthofftype.value=\"%.2s\";t%dthoff.value=\"%.2f\";"
                    "t%den.value=\"%d\";\n",
                    trigger_id,
                    'A' + config.channel,
                    trigger_id,
                    (config.output - IO_PIN_CTRL_CH_A) + 1,
                    trigger_id,
                    config.on_comparator == TriggerConfigEntry::lt ? "lt" : "gt",
                    trigger_id,
                    static_cast<double>(config.on_limit),
                    trigger_id,
                    config.off_comparator == TriggerConfigEntry::lt ? "lt" : "gt",
                    trigger_id,
                    static_cast<double>(config.off_limit),
                    trigger_id,
                    config.enabled ? 1 : 0);

                if (count_would_be_written < 0)
                {
                    // FIXME: Error case, just fill it with NaN
                    const auto written = std::snprintf(
                        temperature_entry_buffer.data(),
                        temperature_entry_buffer.size(),
                        "t%dch.value=\"\";t%dout.value=\"\";t%dthontype.value=\"\";"
                        "t%dthon.value=\"\";t%dthofftype.value=\"\";t%dthoff.value=\"\";"
                        "t%den.value=\"\";",
                        trigger_id,
                        trigger_id,
                        trigger_id,
                        trigger_id,
                        trigger_id,
                        trigger_id,
                        trigger_id);
                    character_count = static_cast<std::size_t>(written);
                    LOG_ERROR("Failed to serialize config values");
                }
                else if (static_cast<std::size_t>(count_would_be_written + 1) >
                         temperature_entry_buffer.size())
                {
                    LOG_WARN("Buffer size insufficient, {} vs {}, resizing...",
                             count_would_be_written + 1,
                             temperature_entry_buffer.size());

                    // Not enough room to process the string, make some more room
                    temperature_entry_buffer.resize(temperature_entry_buffer.size() + 10, '\0');
                    continue;
                }
                else
                {
                    character_count = static_cast<std::size_t>(count_would_be_written);
                }

                break;
            } while (true);

            const auto trigger_setting_js =
                std::string_view{ temperature_entry_buffer.data(), character_count };
            httpd_resp_send_chunk(request,
                                  trigger_setting_js.data(),
                                  static_cast<ssize_t>(trigger_setting_js.size()));
        }
        // End the declaration of the temperature values
        httpd_resp_send_chunk(request, SCRIPT_END.data(), SCRIPT_END.size());

        // Send off the main body of the html document
        const auto html_file = std::string_view{ reinterpret_cast<const char *>(resource_triggers),
                                                 resource_triggers_size };
        httpd_resp_send_chunk(request, html_file.data(), static_cast<ssize_t>(html_file.size()));

        // Close the <html> tag
        httpd_resp_send_chunk(request, HTML_FOOTER.data(), HTML_FOOTER.size());

        // Mark the response as completed
        httpd_resp_send_chunk(request, "", 0);

        return true;
    }

    bool handle_save_triggers(httpd_req_t *request)
    {
        auto revc_buffer = std::array<char, 100>{};
        auto remaining   = request->content_len;
        auto ret         = int{};

        auto body = std::string{};

        while (remaining > 0)
        {
            const auto read_count = std::min<std::size_t>(remaining, sizeof(revc_buffer));

            /* Read the data for the request */
            if ((ret = httpd_req_recv(request, revc_buffer.data(), read_count)) < 0)
            {
                if (ret == HTTPD_SOCK_ERR_TIMEOUT)
                {
                    /* Retry receiving if timeout occurred */
                    continue;
                }
                LOG_ERROR("Failed with error code: {}", ret);
                return false;
            }

            body += std::string_view{ revc_buffer.data(), static_cast<std::size_t>(ret) };
            remaining -= static_cast<std::size_t>(ret);
        }

        body += '&';
        LOG_INFO("Body was: {}", body);
        const auto parse_error = parse_post_body(body);

        static constexpr auto RESPONSE_FMT =
            std::string_view{ "<!DOCTYPE html>\n"
                              "<html>\n"
                              "  <head>\n"
                              "    <meta http-equiv=\"refresh\" content=\"10; "
                              "url='http://192.168.4.1/triggers.html'\" />\n"
                              "  </head>\n"
                              "  <body>\n"
                              "    <h1>{}</h1>\n"
                              "    <h3>Will return to previous page in 10 seconds...</h3>\n"
                              "  </body>\n"
                              "</html>" };
        const auto response = [&] {
            if (!parse_error)
            {
                trigger_config_.save();
                return fmt::format(RESPONSE_FMT, "Success!");
            }

            return fmt::format(RESPONSE_FMT, parse_error.value());
        }();

        // End response
        httpd_resp_send(request, response.data(), static_cast<ssize_t>(response.size()));

        return true;
    }

    std::optional<std::string> parse_post_body(std::string_view content)
    {
        auto result = std::array<TriggerConfigEntry, TRIGGER_COUNT>{};

        std::size_t element_start  = 0;
        std::size_t equalsPosition = std::string::npos;
        for (std::size_t i = 1; i < content.size(); ++i)
        {
            if (content[i] == '=')
            {
                equalsPosition = i;
            }

            if (content[i] == '&')
            {
                if (equalsPosition == std::string::npos)
                {
                    return fmt::format("Failed to parse element at index: {}", element_start);
                }
                else
                {
                    [[maybe_unused]] const auto element =
                        std::string_view{ &content[element_start], i - element_start };
                    const auto key =
                        std::string_view{ &content[element_start], equalsPosition - element_start };
                    const auto value =
                        std::string_view{ &content[equalsPosition + 1], i - equalsPosition - 1 };

                    if (key.size() < 3 || key[0] != 't')
                    {
                        return fmt::format("Unknown setting: {}", key);
                    }
                    else
                    {
                        auto trigger_id = -1;
                        {
                            const auto trigger_id_str = key.substr(0, 2);
                            if (trigger_id_str == "t1")
                            {
                                trigger_id = 0;
                            }
                            else if (trigger_id_str == "t2")
                            {
                                trigger_id = 1;
                            }
                            else if (trigger_id_str == "t3")
                            {
                                trigger_id = 2;
                            }
                            else
                            {
                                return fmt::format("Unknown trigger id: {}", trigger_id_str);
                            }
                        }

                        if (trigger_id != -1)
                        {
                            auto &trigger = result[static_cast<std::size_t>(trigger_id)];

                            const auto trigger_config_entry_field = key.substr(2);
                            if (trigger_config_entry_field == "ch")
                            {
                                if (value == "A")
                                {
                                    trigger.channel = SensorId::A;
                                }
                                else if (value == "B")
                                {
                                    trigger.channel = SensorId::B;
                                }
                                else if (value == "C")
                                {
                                    trigger.channel = SensorId::C;
                                }
                                else if (value == "D")
                                {
                                    trigger.channel = SensorId::D;
                                }
                                else if (value == "E")
                                {
                                    trigger.channel = SensorId::E;
                                }
                                else
                                {
                                    return fmt::format(
                                        "Unknown channel '{}', when parsing trigger '{}'",
                                        value,
                                        trigger_id);
                                }
                            }
                            else if (trigger_config_entry_field == "out")
                            {
                                if (value == "1")
                                {
                                    trigger.output = IO_PIN_CTRL_CH_A;
                                }
                                else if (value == "2")
                                {
                                    trigger.output = IO_PIN_CTRL_CH_B;
                                }
                                else
                                {
                                    return fmt::format(
                                        "Unknown output channel '{}', when parsing trigger '{}'",
                                        value,
                                        trigger_id);
                                }
                            }
                            else if (trigger_config_entry_field == "thontype")
                            {
                                if (value == "lt")
                                {
                                    trigger.on_comparator = TriggerConfigEntry::lt;
                                }
                                else if (value == "gt")
                                {
                                    trigger.on_comparator = TriggerConfigEntry::gt;
                                }
                                else
                                {
                                    return fmt::format(
                                        "Unknown ON comparator '{}', when parsing trigger id '{}'",
                                        value,
                                        trigger_id);
                                }
                            }
                            else if (trigger_config_entry_field == "thon")
                            {
                                if (!value.empty())
                                {
                                    // Convert the string into a floating point number
                                    trigger.on_limit = std::strtof(value.data(), nullptr);
                                }
                            }
                            else if (trigger_config_entry_field == "thofftype")
                            {
                                if (value == "lt")
                                {
                                    trigger.off_comparator = TriggerConfigEntry::lt;
                                }
                                else if (value == "gt")
                                {
                                    trigger.off_comparator = TriggerConfigEntry::gt;
                                }
                                else
                                {
                                    return fmt::format(
                                        "Unknown OFF comparator '{}', when parsing trigger id '{}'",
                                        value,
                                        trigger_id);
                                }
                            }
                            else if (trigger_config_entry_field == "thoff")
                            {
                                if (!value.empty())
                                {
                                    // Convert the string into a floating point number
                                    trigger.off_limit = std::strtof(value.data(), nullptr);
                                }
                            }
                            else if (trigger_config_entry_field == "en")
                            {
                                if (value == "0")
                                {
                                    trigger.enabled = false;
                                }
                                else
                                {
                                    trigger.enabled = true;
                                }
                            }
                            else
                            {
                                return fmt::format(
                                    "Unknown setting '{}', when parsing trigger id '{}'",
                                    trigger_config_entry_field,
                                    trigger_id);
                            }
                        }
                    }
                }

                element_start  = i + 1;
                equalsPosition = std::string::npos;
            }
        }

        for (std::size_t i = 0; i < trigger_config_.size(); ++i)
        {
            result[i].active   = trigger_config_[i].active;
            trigger_config_[i] = result[i];
            LOG_INFO("\n\tTemp Channel: {}\n\tOutput Channel: {}\n\tON Limit: {} "
                     "{}\n\t OFF Limit: "
                     "{} {}\n\tEnabled: {}",
                     trigger_config_[i].channel,
                     trigger_config_[i].output,
                     trigger_config_[i].on_comparator == TriggerConfigEntry::lt ? '<' : '>',
                     trigger_config_[i].on_limit,
                     trigger_config_[i].off_comparator == TriggerConfigEntry::lt ? '<' : '>',
                     trigger_config_[i].off_limit,
                     trigger_config_[i].enabled);
        }

        return std::nullopt;
    }

private:
    HttpServer                   &http_server_;
    TriggerConfig<TRIGGER_COUNT> &trigger_config_;
};

PUMA_TEMPERATURE_MONITOR_END_NAMESPACE