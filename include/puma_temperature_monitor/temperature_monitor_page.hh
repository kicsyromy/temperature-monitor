#pragma once

#include <puma_temperature_monitor/config.hh>
#include <puma_temperature_monitor/http_server.hh>
#include <puma_temperature_monitor/logger.hh>
#include <puma_temperature_monitor/temperature_cache.hh>

#include <resources/resources.h>

#include <fmt/format.h>

#include <string>

PUMA_TEMPERATURE_MONITOR_BEGIN_NAMESPACE

template<std::size_t CACHE_SIZE> class TemperatureMonitorPage
{
public:
    TemperatureMonitorPage(HttpServer &server, TemperatureCache<CACHE_SIZE> &history)
      : http_server_{ server }
      , history_{ history }
    {}

public:
    void register_page()
    {
        http_server_.configure_uri_handler("/", HttpServer::Method::Get, [this](auto *request) {
            return handle_http_get(request);
        });

        http_server_.configure_uri_handler(
            "/index.html",
            HttpServer::Method::Get,
            [this](auto *request) { return handle_http_get(request); });

        http_server_.configure_uri_handler(
            "/temperatures",
            HttpServer::Method::Get,
            [this](auto *request) { return handle_get_temperatures_bin(request); });
    }

private:
    bool handle_http_get(httpd_req_t *request)
    {
        auto temperature_entry_buffer = std::vector<char>{};
        temperature_entry_buffer.resize(50, '\0');

        static constexpr auto HTML_HEADER =
            std::string_view{ "<!DOCTYPE html><html lang=\"en\">\n" };
        static constexpr auto HTML_FOOTER = std::string_view{ "</html>" };

        static constexpr auto SCRIPT_BEGIN =
            std::string_view{ "<script>\nwindow.temperatures=[\n" };
        static constexpr auto SCRIPT_END = std::string_view{ "\n];\n</script>" };

        // Send a proper HTML header
        httpd_resp_send_chunk(request, HTML_HEADER.data(), HTML_HEADER.size());

        // Begin declaring the array of values
        httpd_resp_send_chunk(request, SCRIPT_BEGIN.data(), SCRIPT_BEGIN.size());
        for (const auto &temperatures : history_)
        {
            auto character_count = std::size_t{};
            do
            {
                const auto count_would_be_written =
                    std::snprintf(temperature_entry_buffer.data(),
                                  temperature_entry_buffer.size(),
                                  "{a:%.2f,b:%.2f,c:%.2f,d:%.2f,e:%.2f},\n",
                                  static_cast<double>(temperatures[SensorId::A]),
                                  static_cast<double>(temperatures[SensorId::B]),
                                  static_cast<double>(temperatures[SensorId::C]),
                                  static_cast<double>(temperatures[SensorId::D]),
                                  static_cast<double>(temperatures[SensorId::E]));
                if (count_would_be_written < 0)
                {
                    // FIXME: Error case, just fill it with NaN
                    static constexpr auto DEFAULT_ENTRY =
                        std::string_view{ "{a:NaN,b:NaN,c:NaN,d:NaN,e:NaN},\n" };
                    std::memcpy(temperature_entry_buffer.data(),
                                DEFAULT_ENTRY.data(),
                                DEFAULT_ENTRY.size());
                    character_count = DEFAULT_ENTRY.size();
                    LOG_ERROR("Failed serialize temperature values");
                }
                else if (static_cast<std::size_t>(count_would_be_written + 1) >
                         temperature_entry_buffer.size())
                {
                    LOG_WARN("Buffer size insufficient, {} vs {}, resizing...",
                             count_would_be_written + 1,
                             temperature_entry_buffer.size());

                    // Not enough room to process the string, make some more room
                    temperature_entry_buffer.resize(temperature_entry_buffer.size() + 10, '\0');
                    continue;
                }
                else
                {
                    character_count = static_cast<std::size_t>(count_would_be_written);
                }

                break;
            } while (true);

            const auto temperature_js_object =
                std::string_view{ temperature_entry_buffer.data(), character_count };
            httpd_resp_send_chunk(request,
                                  temperature_js_object.data(),
                                  static_cast<ssize_t>(temperature_js_object.size()));
        }
        // End the declaration of the temperature values
        httpd_resp_send_chunk(request, SCRIPT_END.data(), SCRIPT_END.size());

        // Send off the main body of the html document
        const auto main_html_body =
            std::string_view{ reinterpret_cast<const char *>(resource_index), resource_index_size };
        // LOG_INFO("Sending: {}", main_html_body);
        httpd_resp_send_chunk(request,
                              main_html_body.data(),
                              static_cast<ssize_t>(main_html_body.size()));

        // Close the <html> tag
        httpd_resp_send_chunk(request, HTML_FOOTER.data(), HTML_FOOTER.size());

        // Mark the response as completed
        httpd_resp_send_chunk(request, "", 0);

        return true;
    }

    bool handle_get_temperatures_bin(httpd_req_t *request)
    {
        std::vector<std::int16_t> response{};
        response.reserve(CACHE_SIZE * TemperatureCache<0>::SENSOR_COUNT);

        for (const auto &temperatures : history_)
        {
            // FIXME: Serialize this properly
            response.push_back(static_cast<std::int16_t>(temperatures[SensorId::A] * 100));
            response.push_back(static_cast<std::int16_t>(temperatures[SensorId::B] * 100));
            response.push_back(static_cast<std::int16_t>(temperatures[SensorId::C] * 100));
            response.push_back(static_cast<std::int16_t>(temperatures[SensorId::D] * 100));
            response.push_back(static_cast<std::int16_t>(temperatures[SensorId::E] * 100));
        }

        const auto element_count = static_cast<std::uint16_t>(response.size());

        httpd_resp_set_type(request, "application/binary");

        httpd_resp_send_chunk(request,
                              reinterpret_cast<const char *>(&element_count),
                              sizeof(element_count));
        httpd_resp_send_chunk(
            request,
            reinterpret_cast<const char *>(response.data()),
            static_cast<ssize_t>(response.size() * sizeof(decltype(response)::value_type)));
        httpd_resp_send_chunk(request, "", 0);

        return true;
    }

private:
    HttpServer                   &http_server_;
    TemperatureCache<CACHE_SIZE> &history_;
};

PUMA_TEMPERATURE_MONITOR_END_NAMESPACE