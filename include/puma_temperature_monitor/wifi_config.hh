#pragma once

#include <puma_temperature_monitor/config.hh>

#include <elsen/wifi.hh>

#include <fmt/format.h>

#include <nvs.h>
#include <nvs_flash.h>

#include <array>
#include <optional>
#include <type_traits>

#undef ESP_ERROR_CHECK
#define ESP_ERROR_CHECK(x)                                               \
    do                                                                   \
    {                                                                    \
        if (x != ESP_OK)                                                 \
        {                                                                \
            LOG_ERROR("Failed to interact with NVS, error code: {}", x); \
        }                                                                \
    } while (false)

PUMA_TEMPERATURE_MONITOR_BEGIN_NAMESPACE

struct WiFiConfig
{
    static constexpr auto NVS_NAMESPACE               = "wifi_cfg";
    static constexpr auto NVS_KEY_SSID                = "ssid";
    static constexpr auto NVS_KEY_AUTHENTICATION_MODE = "auth";
    static constexpr auto NVS_KEY_PASSWORD            = "password";

    struct Entry
    {
        std::string                     ssid;
        elsen::wifi::AuthenticationMode authentication_mode;
        std::string                     password;
    };

    static std::optional<Entry> load()
    {
        auto nvs_handle = nvs_handle_t{};

        auto open_result = nvs_open(NVS_NAMESPACE, NVS_READONLY, &nvs_handle);
        if (open_result == ESP_ERR_NVS_NOT_FOUND)
        {
            return std::nullopt;
        }
        ESP_ERROR_CHECK(open_result);

        Entry result{};

        {
            auto       size = std::size_t{};
            const auto err  = nvs_get_blob(nvs_handle, NVS_KEY_SSID, nullptr, &size);
            if (err != ESP_ERR_NVS_NOT_FOUND)
            {
                ESP_ERROR_CHECK(err);
                result.ssid.resize(size, '\0');
                ESP_ERROR_CHECK(nvs_get_blob(nvs_handle, NVS_KEY_SSID, result.ssid.data(), &size));
            }
            else
            {
                return std::nullopt;
            }
        }

        {
            std::uint8_t auth_mode;
            const auto   err = nvs_get_u8(nvs_handle, NVS_KEY_AUTHENTICATION_MODE, &auth_mode);
            if (err != ESP_ERR_NVS_NOT_FOUND)
            {
                ESP_ERROR_CHECK(err);
                result.authentication_mode =
                    static_cast<elsen::wifi::AuthenticationMode>(auth_mode);
            }
            else
            {
                return std::nullopt;
            }
        }

        {
            auto       size = std::size_t{};
            const auto err  = nvs_get_blob(nvs_handle, NVS_KEY_PASSWORD, nullptr, &size);
            if (err != ESP_ERR_NVS_NOT_FOUND)
            {
                ESP_ERROR_CHECK(err);
                result.password.resize(size, '\0');
                ESP_ERROR_CHECK(
                    nvs_get_blob(nvs_handle, NVS_KEY_PASSWORD, result.password.data(), &size));
            }
            else
            {
                return std::nullopt;
            }
        }

        nvs_close(nvs_handle);
        nvs_handle = 0;

        return result;
    }

    static void save(const Entry &entry)
    {
        auto nvs_handle = nvs_handle_t{};

        ESP_ERROR_CHECK(nvs_open(NVS_NAMESPACE, NVS_READWRITE, &nvs_handle));
        ESP_ERROR_CHECK(
            nvs_set_blob(nvs_handle, NVS_KEY_SSID, entry.ssid.data(), entry.ssid.size()));
        ESP_ERROR_CHECK(nvs_set_u8(nvs_handle,
                                   NVS_KEY_AUTHENTICATION_MODE,
                                   static_cast<std::uint8_t>(entry.authentication_mode)));
        ESP_ERROR_CHECK(nvs_set_blob(nvs_handle,
                                     NVS_KEY_PASSWORD,
                                     entry.password.data(),
                                     entry.password.size()));

        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
        nvs_handle = 0;
    }
};

PUMA_TEMPERATURE_MONITOR_END_NAMESPACE
