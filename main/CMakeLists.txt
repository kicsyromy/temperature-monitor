idf_component_register(
	SRCS "main.cc"
	INCLUDE_DIRS "."
)

if (PUMA_TEMPERATURE_MONITOR_ENABLE_TESTING)
	target_link_libraries(${COMPONENT_TARGET} puma_temperature_monitor_test)
else ()
	target_link_libraries(${COMPONENT_TARGET} puma_temperature_monitor)
endif ()