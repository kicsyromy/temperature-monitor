#include <esp_event.h>
#include <esp_timer.h>
#include <esp_wifi.h>
#include <nvs_flash.h>

extern void entrypoint();

inline void initialize_nvs()
{
    auto ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
}

inline void initialize_networking()
{
    ESP_ERROR_CHECK(esp_netif_init());
}

inline void create_main_event_loop()
{
    ESP_ERROR_CHECK(esp_event_loop_create_default());
}

inline void initialize_wifi()
{
    esp_netif_create_default_wifi_ap();
    esp_netif_create_default_wifi_sta();

    const wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
}

extern "C" void app_main()
{
    initialize_nvs();
    initialize_networking();
    create_main_event_loop();
    initialize_wifi();

    entrypoint();
}
